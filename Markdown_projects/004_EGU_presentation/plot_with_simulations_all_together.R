cexmtex <- 0.8
cexmain <- 1.2
#svg(paste0(plotsdir,"/ts_with_simulations.svg"),bg= "transparent", width = 9, height = 11)

#pdf(paste0(plotsdir,"/tssimul.pdf"),bg= "transparent", width = 10, height = 12)
#par(mfrow=c(4,2))

pdf(paste0(plotsdir,"/tssimul_first.pdf"),bg= "transparent", width = 10, height = 6)
par(mfcol=c(2,2))

xlimi = c(mis_transitions[4]*0.001,mis_transitions[3]*0.001)

ylimiTann = c(5,22)
ylimiPann = c(200,1300)

time_mask <- as.numeric(rownames(MNT_prc)) > mis_transitions[3] & as.numeric(rownames(MNT_prc)) < mis_transitions[4]

if (length(which((1:length(rownames(MNT_prc)))[time_mask] == 188))==0){
  ft <- as.numeric(rownames(MNT_prc)[time_mask])*0.001
  
  yT <-MNT.array[time_mask,'Tann',2,1]
  yP <- MNT.array[time_mask,'Pann',2,1]
  
  yTsmooth <- MNT.array[time_mask,'Tann',2,4]
  yPsmooth <- MNT.array[time_mask,'Pann',2,4]

} else {
  new_NA_index <- which((1:length(rownames(MNT_prc)))[time_mask] == 188)
  
  ft <- append(as.numeric(rownames(MNT_prc)[time_mask])*0.001,NA,new_NA_index)
  
  yT <- append(MNT.array[time_mask,'Tann',2,1],NA,new_NA_index)
  yP <- append(MNT.array[time_mask,'Pann',2,1],NA,new_NA_index)
  
  yTsmooth <- append(MNT.array[time_mask,'Tann',2,4],NA,new_NA_index)
  yPsmooth <- append(MNT.array[time_mask,'Pann',2,4],NA,new_NA_index)
}
# Reconstructions plot
par(mar=c(3,4,3,4))
plot(ft,yT , type = "l",axes=TRUE,ylab="",xlim=xlimi,ylim=ylimiTann,col=alpha(colT,alphats),xlab='',main ='Lago Grande di Monticchio, mean annual temperature', cex.main = cexmain)
lines(ft,yTsmooth,col=colT)

arrows(14,Monticchio$Temp_ClimExpl['T','Tann']+Monticchio$Temp_ClimExpl['sd','Tann'],
       14,Monticchio$Temp_ClimExpl['T','Tann']-Monticchio$Temp_ClimExpl['sd','Tann'],
       col=colT,lwd=2,code = 3,angle=90)
mtext("[°C]", side = 2, col = colT, line = 2, cex = cexmtex)
abline(h = mean(yT, na.rm = T), col = alpha(colT,alphamean), lty = 6)
time <- abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]]))*0.001

# add loveclim
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["MNT","Tann",,1], 'l', xlim= xlimi, col = alpha("coral",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["MNT","Tann",,"smoothed"], col = "coral", xlim = xlimi)
abline(h = mean(array.LOVECLIMMis3["MNT","Tann",,1]), col = alpha("coral",alphamean), lty = 6)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("black","coral"), lty=1, bty='n')

grid()

# par(new = TRUE)
# Prec pot
plot(ft, yP, col = alpha(colP,alphats), type = "l", axes = T, xlab = "", ylab = "",xlim=xlimi, ylim=ylimiPann,
     main ='Lago Grande di Monticchio, annual precipitation',cex.main = cexmain)
lines(ft,yPsmooth,col=colP)
arrows(14,Monticchio$Prec_ClimExpl['P','Pann']+Monticchio$Prec_ClimExpl['sd','Pann'],
       14,Monticchio$Prec_ClimExpl['P','Pann']-Monticchio$Prec_ClimExpl['sd','Pann'],
       col=colP,lwd=2,code = 3,angle=90)
axis(2, col.axis = colP, col = colP)
mtext(side = 2,"[mm]" ,col = colP, padj = -4,cex = cexmtex)
abline(h = mean(yP, na.rm = T), col = alpha(colP,alphamean), lty = 6)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)

lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["MNT","Pann",,1], 'l', xlim= xlimi,ylim=ylimiPann, col = alpha("darkcyan",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["MNT","Pann",,"smoothed"], col = "darkcyan", xlim = xlimi, ylim=ylimiPann)
abline(h = mean(array.LOVECLIMMis3["MNT","Pann",,1]), col = alpha("darkcyan",alphamean), lty = 6)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("blue","darkcyan"), lty=1, bty='n')

grid()

### Plotting Tulane!

ylimiTann = c(10,30)
ylimiPann = c(800,2300)

time_mask <- as.numeric(rownames(TUL_prc)) > mis_transitions[3] & as.numeric(rownames(TUL_prc)) < mis_transitions[4]

ft <- append(as.numeric(rownames(TUL_prc)[time_mask])*0.001,NA,after=which(TUL_meta$ISR[time_mask]>1500))

yT <- append(TUL.array[time_mask,'Tann',2,1],NA,after=which(TUL_meta$ISR[time_mask]>1500))
yP <- append(TUL.array[time_mask,'Pann',2,1],NA,after=which(TUL_meta$ISR[time_mask]>1500))

yTsmooth <- append(TUL.array[time_mask,'Tann',2,4],NA,after=which(TUL_meta$ISR[time_mask]>1500))
yPsmooth <- append(TUL.array[time_mask,'Pann',2,4],NA,after=which(TUL_meta$ISR[time_mask]>1500))

par(mar=c(3,4,3,4))
plot(ft,yT , type = "l",axes=TRUE,ylab="",xlim=xlimi,ylim=ylimiTann ,col=alpha(colT,alphats),xlab='',main ='Lake Tulane, mean annual temperature'
     ,cex.main = cexmain)
lines(ft,yTsmooth,col=colT)

arrows(14,TUL_meta$Temp_CRU$Tann+1,
       14,TUL_meta$Temp_CRU$Tann-1,
       col=colT,lwd=2,code = 3,angle=90)
mtext("[°C]", side = 2, col = colT, line = 2,cex = cexmtex)
abline(h = mean(yT, na.rm = T), col = alpha(colT,alphamean), lty = 6)
points(14,TUL_meta$Temp_CRU$Tann, col= colT)

# add loveclim
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["TUL","Tann",,1], 'l', xlim= xlimi, col = alpha("coral",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["TUL","Tann",,"smoothed"], col = "coral", xlim = xlimi)
abline(h = mean(array.LOVECLIMMis3["TUL","Tann",,1]), col = alpha("coral",alphamean), lty = 6)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)
grid()
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("black","coral"), lty=1, bty='n')

#par(new = TRUE)

plot(ft, yP, col = alpha(colP,alphats), type = "l", axes = T, xlab = "", ylab = "",xlim=xlimi,ylim = ylimiPann, main = 'Lake Tulane, annual precipitation', cex.main = cexmain)
lines(ft,yPsmooth,col=colP)
arrows(14,TUL_meta$Prec_CRU$Pann[1] +TUL_meta$Prec_CRU$Pann[2],
       14,TUL_meta$Prec_CRU$Pann[1]-TUL_meta$Prec_CRU$Pann[2],
       col=colP,lwd=2,code = 3,angle=90)
axis(2, col.axis = colP, col = colP)
mtext(side = 2,"[mm]" ,col = colP, padj = -4,cex = cexmtex)
abline(h = mean(yP, na.rm = T), col = alpha(colP,alphamean), lty = 6)
points(14,TUL_meta$Prec_CRU$Pann[1], col= colP)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)

lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["TUL","Pann",,1], 'l', xlim= xlimi,ylim=ylimiPann, col = alpha("darkcyan",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["TUL","Pann",,"smoothed"], col = "darkcyan", xlim = xlimi, ylim=ylimiPann)
abline(h = mean(array.LOVECLIMMis3["TUL","Pann",,1]), col = alpha("darkcyan",alphamean), lty = 6)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("blue","darkcyan"), lty=1, bty='n')

grid()

dev.off()
pdf(paste0(plotsdir,"/tssimul_second.pdf"),bg= "transparent", width = 10, height = 6)
par(mfcol=c(2,2))
### Plotting Little Lake!

ylimiTann = c(3,18)
ylimiPann = c(200,2100)

time_mask <- as.numeric(rownames(LIT_prc)) > mis_transitions[3] & as.numeric(rownames(LIT_prc)) < mis_transitions[4]

ft <- as.numeric(rownames(LIT_prc)[time_mask])*0.001
yT <- LIT.array[time_mask,'Tann',2,1]
yP <- LIT.array[time_mask,'Pann',2,1]

yTsmooth <- LIT.array[time_mask,'Tann',2,4]
yPsmooth <- LIT.array[time_mask,'Pann',2,4]

par(mar=c(3,4,3,4))

# 
plot(ft,yT , type = "l",axes=TRUE,ylab="",xlim=xlimi,ylim=ylimiTann ,col=alpha(colT,alphats),xlab='',main ='Little Lake, mean annual temperatue', cex.main = cexmain)
lines(ft,yTsmooth,col=colT)

arrows(14,LIT_meta$Temp_CRU$Tann+1,
       14,LIT_meta$Temp_CRU$Tann-1,
       col=colT,lwd=2,code = 3,angle=90)
mtext("[°C]", side = 2, col = colT, line = 2,cex = cexmtex)
abline(h = mean(yT, na.rm = T), col = alpha(colT,alphamean), lty = 6)
points(14,LIT_meta$Temp_CRU$Tann, col= colT)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)

#Temp Loveclim
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["LIT","Tann",,1], 'l', xlim= xlimi,ylim=ylimiTann, col = alpha("coral",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["LIT","Tann",,"smoothed"], col = "coral", xlim = xlimi, ylim=ylimiTann)
abline(h = mean(array.LOVECLIMMis3["LIT","Tann",,1]), col = alpha("coral",alphamean), lty = 6)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("black","coral"), lty=1, bty='n')

grid()


#par(new = TRUE)


# prec
plot(ft, yP, col = alpha(colP,alphats), type = "l", axes = T, xlab = "", ylab = "",xlim=xlimi,ylim = ylimiPann, main = 'Little Lake, annual precipitation',cex.main = cexmain)
lines(ft,yPsmooth,col=colP)
arrows(14,LIT_meta$Prec_CRU$Pann[1] +LIT_meta$Prec_CRU$Pann[2],
       14,LIT_meta$Prec_CRU$Pann[1]-LIT_meta$Prec_CRU$Pann[2],
       col=colP,lwd=2,code = 3,angle=90)
axis(2, col.axis = colP, col = colP)
mtext(side = 2,"[mm]" ,col = colP, padj = -4,cex = cexmtex)
abline(h = mean(yP, na.rm = T), col = alpha(colP,alphamean), lty = 6)
points(14,LIT_meta$Prec_CRU$Pann[1], col= colP)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)

#Prec Loveclim
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["LIT","Pann",,1], 'l', xlim= xlimi,ylim=ylimiPann, col = alpha("darkcyan",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["LIT","Pann",,"smoothed"], col = "darkcyan", xlim = xlimi, ylim=ylimiPann)
abline(h = mean(array.LOVECLIMMis3["LIT","Pann",,1]), col = alpha("darkcyan",alphamean), lty = 6)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("blue","darkcyan"), lty=1, bty='n')

grid()

### Plotting Fargher!

ylimiTann = c(1,12)
ylimiPann = c(400,1800)

time_mask <- as.numeric(rownames(FAR_prc)) > mis_transitions[3] & as.numeric(rownames(FAR_prc)) < mis_transitions[4]

ft <- append(as.numeric(rownames(FAR_prc)[time_mask])*0.001,NA,after=which(FAR_meta$ISR[time_mask]>1500))

yT <- append(FAR.array[time_mask,'Tann',2,1],NA,after=which(FAR_meta$ISR[time_mask]>1500))
yP <- append(FAR.array[time_mask,'Pann',2,1],NA,after=which(FAR_meta$ISR[time_mask]>1500))

yTsmooth <- append(FAR.array[time_mask,'Tann',2,4],NA,after=which(FAR_meta$ISR[time_mask]>1500))
yPsmooth <- append(FAR.array[time_mask,'Pann',2,4],NA,after=which(FAR_meta$ISR[time_mask]>1500))

par(mar=c(3,4,3,4))
plot(ft,yT , type = "l",axes=TRUE,ylab="",xlim=xlimi,ylim=ylimiTann ,col=alpha(colT,alphats),xlab='',main ='Fargher Lake, mean annual temperature',cex.main = cexmain)
lines(ft,yTsmooth,col=colT)

arrows(14,FAR_meta$Temp_CRU$Tann+1,
       14,FAR_meta$Temp_CRU$Tann-1,
       col=colT,lwd=2,code = 3,angle=90)
mtext("[°C]", side = 2, col = colT, line = 2,cex = cexmtex)
abline(h = mean(yT, na.rm = T), col = alpha(colT,alphamean), lty = 6)
points(14,FAR_meta$Temp_CRU$Tann, col= colT)

# add loveclim
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["FAR","Tann",,1], 'l', xlim= xlimi, col = alpha("coral",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["FAR","Tann",,"smoothed"], col = "coral", xlim = xlimi)
abline(h = mean(array.LOVECLIMMis3["FAR","Tann",,1]), col = alpha("coral",alphamean), lty = 6)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)
grid()
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("black","coral"), lty=1, bty='n')

#par(new = TRUE)

plot(ft, yP, col = alpha(colP,alphats), type = "l", axes = T, xlab = "", ylab = "",xlim=xlimi,ylim = ylimiPann, main = 'Fargher Lake, annual precipitation',cex.main = cexmain)
lines(ft,yPsmooth,col=colP)
arrows(14,FAR_meta$Prec_CRU$Pann[1] +FAR_meta$Prec_CRU$Pann[2],
       14,FAR_meta$Prec_CRU$Pann[1]-FAR_meta$Prec_CRU$Pann[2],
       col=colP,lwd=2,code = 3,angle=90)
axis(2, col.axis = colP, col = colP)
mtext(side = 2,"[mm]" ,col = colP, padj = -4,cex = cexmtex)
abline(h = mean(yP, na.rm = T), col = alpha(colP,alphamean), lty = 6)
points(14,FAR_meta$Prec_CRU$Pann[1], col= colP)
mtext(side = 1,"ka BP" ,col = 'black', padj = 3,cex = cexmtex)

lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])), array.LOVECLIMMis3["FAR","Pann",,1], 'l', xlim= xlimi,ylim=ylimiPann, col = alpha("darkcyan",0.2))
lines(abs(as.numeric(dimnames(array.LOVECLIMMis3)[[3]])),array.LOVECLIMMis3["FAR","Pann",,"smoothed"], col = "darkcyan", xlim = xlimi, ylim=ylimiPann)
abline(h = mean(array.LOVECLIMMis3["FAR","Pann",,1]), col = alpha("darkcyan",alphamean), lty = 6)
legend("topleft", legend = c("Proxy reconstruction (WAPLS)","LOVECLIM"), col = c("blue","darkcyan"), lty=1, bty='n')
grid()

dev.off()
