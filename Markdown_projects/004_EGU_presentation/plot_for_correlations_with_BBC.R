pdf(paste0(plotsdir,"/corrs_with_BBC.pdf"),bg= "transparent", width = 7, height = 9)
proxycol <- 'red'
proxysymb <- 1
modelsymb <- 3
modelsymb2 <- 8
modelcol <- 'blue'
modelcol2 <- "darkturquoise"
par(mar=c(4.1, 4.1, 2.1, 1.1), las = 0)
plot(MNT.proxy.insight[-8]+7,xaxt='n', ylim = c(0,8), yaxt='n',xlab='',ylab='', col=proxycol, pch=proxysymb, main = 'Proxy and model T-P correlations at different timescales', yaxs="i",
     cex.main = 1.2)

abline(h = 6, col= alpha("black",0.8))
abline(h = 4,col= alpha("black",0.8))
abline(h = 2, col= alpha("black",0.8))
abline(h = 7, col= alpha("black",0.2), lty=2)
abline(h = 5, col= alpha("black",0.2), lty=2)
abline(h = 3, col= alpha("black",0.2), lty=2)
abline(h = 1, col= alpha("black",0.2), lty=2)

abline(v = 1, col= alpha("black",0.1),lty=2)
abline(v = 2, col= alpha("black",0.1),lty=2)
abline(v = 3, col= alpha("black",0.1),lty=2)
abline(v = 4, col= alpha("black",0.1),lty=2)
abline(v = 5, col= alpha("black",0.1),lty=2)
abline(v = 6, col= alpha("black",0.1),lty=2)
abline(v = 7, col= alpha("black",0.1),lty=2)


points(corels.in.loveclim["MNT",-8,]+7,xaxt='n', col= modelcol,  pch=modelsymb)
#lines(corels.in.loveclim["MNT",-8,]+7, col= alpha(modelcol,0.2),  lty=2)
points(corels.in.loveclim["TUL",-8,]+5,xaxt='n', col= modelcol,  pch=modelsymb)
points(corels.in.loveclim["LIT",-8,]+3,xaxt='n', col= modelcol,  pch=modelsymb)
points(corels.in.loveclim["FAR",-8,]+1,xaxt='n', col= modelcol,  pch=modelsymb)

points(c(NA,NA,corels.in.BBC["MNT",-6,]+7),xaxt='n', col= modelcol2,  pch=modelsymb2)
#lines(corels.in.loveclim["MNT",-8,]+7, col= alpha(modelcol,0.2),  lty=2)
points(c(NA,NA,corels.in.BBC["TUL",-6,]+5),xaxt='n', col= modelcol2,  pch=modelsymb2)
points(c(NA,NA,corels.in.BBC["LIT",-6,]+3),xaxt='n', col= modelcol2,  pch=modelsymb2)
points(c(NA,NA,corels.in.BBC["FAR",-6,]+1),xaxt='n', col= modelcol2,  pch=modelsymb2)


points(TUL.proxy.insight[-8]+5,xaxt='n', ylim = c(0,8), yaxt='n',xlab='',ylab='', col=proxycol, pch=proxysymb )
points(LIT.proxy.insight[-8]+3,xaxt='n', ylim = c(0,8), yaxt='n',xlab='',ylab='', col=proxycol, pch=proxysymb )
points(FAR.proxy.insight[-8]+1,xaxt='n', ylim = c(0,8), yaxt='n',xlab='',ylab='', col=proxycol, pch=proxysymb )

axis(1,at=1:7,labels=paste(dimnames(insight.LOVECLIMMis3)[[4]][-8]))

mtext(side=1,"Timescale [ka]",padj = 3,cex = 1.3)

axis(2,at=c(1,3,5,7),labels=c("Fargher","Little","Tulane","Monticchio"), tick = F, las=0, padj = -1.5, cex.axis=1.3)
#axis(2,at=c(1,3,5,7), labels = F)
axis(2,at=c(1,3,5,7), labels = c(0,0,0,0), tick = T, padj = 1,cex.axis = 0.8)
axis(2,at = c(1,3,5,7)+0.8, labels = rep("0.8",4),tick = T, padj = 1, cex.axis = 0.8)
axis(2,at = c(1,3,5,7)-0.8, labels = rep("-0.8",4),tick = T, padj = 1,cex.axis = 0.8)
legend('bottomleft',legend = c('Proxy reconstruction','LOVECLIM grid point',"BBC grid point"), col=c("red","blue","darkturquoise"),pch = c(1,3,8),
       bty='n', cex = 1.2)

dev.off()


