# comp <- 1
# source(paste0(projectdir,"/proxy_insight_script2.R"))
# 
# plot(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), main=paste0(comp) )
# lines(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('black',0.2) )
# points(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), col="Maroon" )
# lines(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('Maroon',0.2) )
# points(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), col="darkblue" )
# lines(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('darkblue',0.2) )
# points(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), col="coral" )
# lines(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('coral',0.2) )
# axis(1,at=1:length(low_filters),labels=as.character(low_filters))

comp <- 2
source(paste0(projectdir,"/proxy_insight_script2.R"))

plot(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), main=paste0(comp) )
lines(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('black',0.2) )
points(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), col="Maroon" )
lines(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('Maroon',0.2) )
points(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), col="darkblue" )
lines(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('darkblue',0.2) )
points(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), col="coral" )
lines(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('coral',0.2) )
axis(1,at=1:length(low_filters),labels=as.character(low_filters))

# comp <- 3
# source(paste0(projectdir,"/proxy_insight_script2.R"))
# 
# plot(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), main=paste0(comp) )
# lines(MNT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('black',0.2) )
# points(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), col="Maroon" )
# lines(TUL.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('Maroon',0.2) )
# points(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), col="darkblue" )
# lines(LIT.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('darkblue',0.2) )
# points(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), col="coral" )
# lines(FAR.proxy.insight,xaxt='n', ylim = c(-1,1), lty = 2, col=alpha('coral',0.2) )
# axis(1,at=1:length(low_filters),labels=as.character(low_filters))