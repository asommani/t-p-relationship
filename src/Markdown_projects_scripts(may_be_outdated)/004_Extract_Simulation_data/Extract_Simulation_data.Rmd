---
title: "extract_simulation_data"
author: "Anna"
date: "4/27/2020"
output: html_document
---
## In this Markdown file I want to extract all the simulation data for my fossil locations!

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
# Basedrvie 
#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
#basedrive = '/Users/annarella/Dropbox'
projectdir <- paste0(basedrive,"/sirius/temp/Extract_Simulation_data")
plotsdir <- paste0(projectdir,"/plots")
if (!file.exists(plotsdir)){
  dir.create(file.path(plotsdir))
}

'%!in%' <- function(x,y)!('%in%'(x,y))

```

```{r libraries}
library(ncdf4)
library(geosphere)
```


```{r coordinate of fossil pollen cores}

coord_FAR <- c(-122.519,45.886, 200)
coord_LIT <- c(-123.584,44.168, 217)
coord_MNT <- c(15.605,40.931,656)
coord_SHL <- c(126.6,42.286,797)  #lon, lat, elevation
coord_TUL <- c(-81.504,27.586, 36) 
```

### Messages that Nils sent me:
The main library for reading the simulation data is library(ncdf4)


## LOVECLIM MIS3
For the LOVECLIM MIS3 simulation (with DO events):
```{r}
#mis3
nc <- nc_open("/stacydata/data/LOVECLIM/mis3/annual_surface_temperature.nc")
lon <- ncvar_get(nc,"LONN31_33")
lat <- ncvar_get(nc,"LAT")
time <- ncvar_get(nc,"TAX")-50000 #("YRS AFTER 50KA BP"?, it says "YRS AFTER 30KA BP" which is clearly wrong, I might be 200yr off)
temperature <- ncvar_get(nc,"TS")
nc <- nc_open("/stacydata/data/LOVECLIM/mis3/annual_total_precipitation.nc")
precip <- ncvar_get(nc,"PP")*10

```
These are just mean annual temperatures
Unfortunately seasonal temperatures / precip are not publicly available

```{r LOVECLIM MIS3 miei appunti}
range(time) # -50000 -29801 #it covers MIS3
length(time) #20200 # one mean per year

range(lon) # -180 180
length(lon) # 65

range(lat) # -85.76059  85.76059
length(lat) # 32

dim(temperature) # 65    32 20200
dim(precip) # 65    32 20200

lon[-1]-lon[-65] #delta = 5.625
summary(lat[-1]-lat[-32]) # delta ~5.537, meno preciso all'equatore...
```
First dimention is Longitude, then Latitude then years
```{r}
dimnames(temperature)[[1]] <- lon
dimnames(temperature)[[2]] <- lat
dimnames(temperature)[[3]] <- time

dimnames(precip)[[1]] <- lon
dimnames(precip)[[2]] <- lat
dimnames(precip)[[3]] <- time
```

Cheking which points are the closest to my fossil pollen sites
```{r}
set_of_simulation_points <- as.matrix(data.frame(lon = rep(lon, each = length(lat)), lat = rep(lat, length(lon))))
sort(distGeo(set_of_simulation_points, coord_FAR[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_LIT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_MNT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_SHL[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_TUL[1:2]))[1:10]
```

Extract the data t the closest point
```{r}

# c("Longitude","Latitude","Distance")
sim_point_FAR <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_FAR[1:2])),],min(distGeo(set_of_simulation_points, coord_FAR[1:2])))
sim_point_LIT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_LIT[1:2])),],min(distGeo(set_of_simulation_points, coord_LIT[1:2])))
sim_point_MNT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_MNT[1:2])),],min(distGeo(set_of_simulation_points, coord_MNT[1:2])))
sim_point_SHL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_SHL[1:2])),],min(distGeo(set_of_simulation_points, coord_SHL[1:2])))
sim_point_TUL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_TUL[1:2])),],min(distGeo(set_of_simulation_points, coord_TUL[1:2])))

# creating arrays of simulation and coordinates
LOVECLIM_MIS3 <- array(dim = c(5,2,length(time)))
dimnames(LOVECLIM_MIS3)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(LOVECLIM_MIS3)[[2]] <- c("Tann","Pann")
dimnames(LOVECLIM_MIS3)[[3]] <- time
coord_LOVECLIM_MIS3 <- array(dim = c(5,3))
dimnames(coord_LOVECLIM_MIS3)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(coord_LOVECLIM_MIS3)[[2]] <- c("Longitude","Latitude","Distance")
coord_LOVECLIM_MIS3["FAR",] <- sim_point_FAR
coord_LOVECLIM_MIS3["LIT",] <- sim_point_LIT
coord_LOVECLIM_MIS3["MNT",] <- sim_point_MNT
coord_LOVECLIM_MIS3["SHL",] <- sim_point_SHL
coord_LOVECLIM_MIS3["TUL",] <- sim_point_TUL


LOVECLIM_MIS3["FAR","Tann",] <- temperature[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]
LOVECLIM_MIS3["FAR","Pann",] <- precip[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]

LOVECLIM_MIS3["LIT","Tann",] <- temperature[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]
LOVECLIM_MIS3["LIT","Pann",] <- precip[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]

LOVECLIM_MIS3["MNT","Tann",] <- temperature[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]
LOVECLIM_MIS3["MNT","Pann",] <- precip[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]

LOVECLIM_MIS3["SHL","Tann",] <- temperature[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]
LOVECLIM_MIS3["SHL","Pann",] <- precip[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]

LOVECLIM_MIS3["TUL","Tann",] <- temperature[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]
LOVECLIM_MIS3["TUL","Pann",] <- precip[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]

list_loveclim_mis3 <- list()
list_loveclim_mis3[[1]] <- LOVECLIM_MIS3
list_loveclim_mis3[[2]] <- coord_LOVECLIM_MIS3
names(list_loveclim_mis3) <- c("LOVECLIM_MIS3","coord_LOVECLIM_MIS3")
#save(list_loveclim_mis3, file=paste0(projectdir,"/LOVECLIM_MIS3.RData"))
```



**LOVECLIM 800kyr**
For the 800kyr LOVECLIM run, data is only available every 1000 years (and only mean annual temp/precip):
```{r}
#800k
nc <- nc_open("/stacydata/data/LOVECLIM/800k/precipitation.nc")
precip <- ncvar_get(nc,"PRECIP")*10
nc <- nc_open("/stacydata/data/LOVECLIM/800k/surface_air_temperature.nc")
lon <- ncvar_get(nc,"LONN31_33")
lat <- ncvar_get(nc,"LAT")
time <- (ncvar_get(nc,"TAX")-783) * 1000 #("YRS AFTER 783K BP", actually "kyr after 783ka")
temperature <- ncvar_get(nc,"TSURF")
```


```{r LOVECLIM 800kyr miei appunti}
range(time) # -783000 0 # last 800kyr
length(time) # 784 #only one data each 1000yr

range(lon) # -180 180
length(lon) # 65

range(lat) # -85.76059  85.76059
length(lat) # 32

dim(temperature) # 65    32 784
dim(precip) # 65    32 784
```

First dimention is Longitude, then Latitude then years
```{r}
dimnames(temperature)[[1]] <- lon
dimnames(temperature)[[2]] <- lat
dimnames(temperature)[[3]] <- time

dimnames(precip)[[1]] <- lon
dimnames(precip)[[2]] <- lat
dimnames(precip)[[3]] <- time
```

Cheking which points are the closest to my fossil pollen sites
```{r}
set_of_simulation_points <- as.matrix(data.frame(lon = rep(lon, each = length(lat)), lat = rep(lat, length(lon))))
sort(distGeo(set_of_simulation_points, coord_FAR[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_LIT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_MNT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_SHL[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_TUL[1:2]))[1:10]
```

Extract the data t the closest point (Same as for LOVECLIM MIS3)
```{r}

# c("Longitude","Latitude","Distance")
sim_point_FAR <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_FAR[1:2])),],min(distGeo(set_of_simulation_points, coord_FAR[1:2])))
sim_point_LIT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_LIT[1:2])),],min(distGeo(set_of_simulation_points, coord_LIT[1:2])))
sim_point_MNT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_MNT[1:2])),],min(distGeo(set_of_simulation_points, coord_MNT[1:2])))
sim_point_SHL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_SHL[1:2])),],min(distGeo(set_of_simulation_points, coord_SHL[1:2])))
sim_point_TUL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_TUL[1:2])),],min(distGeo(set_of_simulation_points, coord_TUL[1:2])))
```

Take the simulation points I am interest in 
```{r}

# creating arrays of simulation and coordinates
LOVECLIM_800kyrs <- array(dim = c(5,2,length(time)))

dimnames(LOVECLIM_800kyrs)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(LOVECLIM_800kyrs)[[2]] <- c("Tann","Pann")
dimnames(LOVECLIM_800kyrs)[[3]] <- time

coord_LOVECLIM_800kyrs <- array(dim = c(5,3))

dimnames(coord_LOVECLIM_800kyrs)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(coord_LOVECLIM_800kyrs)[[2]] <- c("Longitude","Latitude","Distance")

coord_LOVECLIM_800kyrs["FAR",] <- sim_point_FAR
coord_LOVECLIM_800kyrs["LIT",] <- sim_point_LIT
coord_LOVECLIM_800kyrs["MNT",] <- sim_point_MNT
coord_LOVECLIM_800kyrs["SHL",] <- sim_point_SHL
coord_LOVECLIM_800kyrs["TUL",] <- sim_point_TUL


LOVECLIM_800kyrs["FAR","Tann",] <- temperature[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]
LOVECLIM_800kyrs["FAR","Pann",] <- precip[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]

LOVECLIM_800kyrs["LIT","Tann",] <- temperature[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]
LOVECLIM_800kyrs["LIT","Pann",] <- precip[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]

LOVECLIM_800kyrs["MNT","Tann",] <- temperature[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]
LOVECLIM_800kyrs["MNT","Pann",] <- precip[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]

LOVECLIM_800kyrs["SHL","Tann",] <- temperature[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]
LOVECLIM_800kyrs["SHL","Pann",] <- precip[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]

LOVECLIM_800kyrs["TUL","Tann",] <- temperature[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]
LOVECLIM_800kyrs["TUL","Pann",] <- precip[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]

list_loveclim_800kyrs <- list()
list_loveclim_800kyrs[[1]] <- LOVECLIM_800kyrs
list_loveclim_800kyrs[[2]] <- coord_LOVECLIM_800kyrs
names(list_loveclim_800kyrs) <- c("LOVECLIM_800kyrs","coord_LOVECLIM_800kyrs")
#save(list_loveclim_800kyrs, file=paste0(projectdir,"/LOVECLIM_800kyrs.RData"))
```



**BBC**
The BBC data is a bit more complicated. We have simulations for every 2-4kyr, which are each around 1000 years with constant boundary conditions (but at least we have monthly data...). I have a processed dataset of temperature (annual, jja, djf), precip (annual), and pfts. You can start with this ones and we can extract more if necessary.
The timesteps are
```{r}
sim_dates <- c(seq(120,80,by=-4),seq(78,22,by=-2),seq(21,0,by=-1))
```
And the data is here:
```{r}
load("/home/sirius/Data/bridge_data.RData")
```
bridge_data loads a series of array:

bridge_pft
bridge_precip
bridge_ts
bridge_ts_djf
bridge_ts_jja

The temperature is given in Kelvin!
```{r}
dim(bridge_pft) # 62 96 73  9
dim(bridge_precip) # 62 96 73
dim(bridge_ts) # 62 96 73
dim(bridge_ts_djf) # 62 96 73
dim(bridge_ts_jja) # 62 96 73
length(sim_dates) # 62
```
```{r}
nc <- nc_open("/stacydata/data/BRIDGE/bbc_all_triff_rec_dyn04/teii01.temp_mm_srf.monthly.nc")
lat <- rev(ncvar_get(nc,"latitude"))
lon <- ncvar_get(nc,"longitude")
length(lat)
length(lon)
```
I have no idea what bridge_pft is, but the other arrays loaded (bridge_...) are given in
*time longitude latitude*
```{r}
dimnames(bridge_precip)[[1]] <- sim_dates
dimnames(bridge_precip)[[2]] <- lon
dimnames(bridge_precip)[[3]] <- lat

dimnames(bridge_ts)[[1]] <- sim_dates
dimnames(bridge_ts)[[2]] <- lon
dimnames(bridge_ts)[[3]] <- lat

dimnames(bridge_ts_djf)[[1]] <- sim_dates
dimnames(bridge_ts_djf)[[2]] <- lon
dimnames(bridge_ts_djf)[[3]] <- lat

dimnames(bridge_ts_jja)[[1]] <- sim_dates
dimnames(bridge_ts_jja)[[2]] <- lon
dimnames(bridge_ts_jja)[[3]] <- lat

# Plant functional types
dimnames(bridge_pft)[[1]] <- sim_dates
dimnames(bridge_pft)[[2]] <- lon
dimnames(bridge_pft)[[3]] <- lat
dimnames(bridge_pft)[[4]] <- c("Broadleaf tree [Tropical]", "Needleleaf tree", "C3 grass"," C4 grass", "Shrub",  "Urban", "Inland water", "Bare soil", "Ice")

```

```{r}
transform_longitudes <- function(lons){
    tlons <- rep(NA,length(lons))
    for (i in 1:length(lons)){
    if (lons[i] <= 180){
      tlons[i] = lons[i]
    } else {tlons[i] = lons[i]-360}
    }
return(tlons)}

tlon <- transform_longitudes(lon)
```


```{r}
set_of_simulation_points <- as.matrix(data.frame(lon = rep(tlon, each = length(lat)), lat = rep(lat, length(lon))))
sort(distGeo(set_of_simulation_points, coord_FAR[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_LIT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_MNT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_SHL[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_TUL[1:2]))[1:10]
```
```{r}
sim_point_FAR <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_FAR[1:2])),],min(distGeo(set_of_simulation_points, coord_FAR[1:2]))) # same one in this case
sim_point_LIT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_LIT[1:2])),],min(distGeo(set_of_simulation_points, coord_LIT[1:2]))) # same one in this case
sim_point_MNT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_MNT[1:2])),],min(distGeo(set_of_simulation_points, coord_MNT[1:2])))
sim_point_SHL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_SHL[1:2])),],min(distGeo(set_of_simulation_points, coord_SHL[1:2])))
sim_point_TUL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_TUL[1:2])),],min(distGeo(set_of_simulation_points, coord_TUL[1:2])))
```
correct the long
```{r}
sim_point_FAR[1] <- 123.75+180
sim_point_LIT[1] <- 123.75+180
sim_point_TUL[1] <- 82.5+180
```

Take the simulation points I am interest in 
```{r}
#c("Broadleaf tree [Tropical]", "Needleleaf tree", "C3 grass"," C4 grass", "Shrub",  "Urban", "Inland water", "Bare soil", "Ice")
# creating arrays of simulation and coordinates
BBC <- array(dim = c(5,4,length(sim_dates)))

dimnames(BBC)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(BBC)[[2]] <- c("Tann","Pann","Tdjf","Tjja")
dimnames(BBC)[[3]] <- sim_dates

coord_BBC <- array(dim = c(5,3))

dimnames(coord_BBC)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(coord_BBC)[[2]] <- c("Longitude","Latitude","Distance")

```

```{r}
secs_in_a_year <- 3600*24*360 #360 days per year in the simulation!
# Density of water
```


```{r}

coord_BBC["FAR",] <- sim_point_FAR
coord_BBC["LIT",] <- sim_point_LIT
coord_BBC["MNT",] <- sim_point_MNT
coord_BBC["SHL",] <- sim_point_SHL
coord_BBC["TUL",] <- sim_point_TUL


BBC["FAR","Tann",] <- -273.15 + bridge_ts[,as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2])]
BBC["FAR","Pann",] <- secs_in_a_year * bridge_precip[,as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2])]
BBC["FAR","Tdjf",] <- -273.15 + bridge_ts_djf[,as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2])]
BBC["FAR","Tjja",] <- -273.15 + bridge_ts_jja[,as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2])]



BBC["LIT","Tann",] <- -273.15 + bridge_ts[,as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2])]
BBC["LIT","Pann",] <- secs_in_a_year * bridge_precip[,as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2])]
BBC["LIT","Tdjf",] <- -273.15 + bridge_ts_djf[,as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2])]
BBC["LIT","Tjja",] <- -273.15 + bridge_ts_jja[,as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2])]


BBC["MNT","Tann",] <- -273.15 + bridge_ts[,as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2])]
BBC["MNT","Pann",] <- secs_in_a_year * bridge_precip[,as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2])]
BBC["MNT","Tdjf",] <- -273.15 + bridge_ts_djf[,as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2])]
BBC["MNT","Tjja",] <- -273.15 + bridge_ts_jja[,as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2])]


BBC["SHL","Tann",] <- -273.15 + bridge_ts[,as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2])]
BBC["SHL","Pann",] <- secs_in_a_year * bridge_precip[,as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2])]
BBC["SHL","Tdjf",] <- -273.15 + bridge_ts_djf[,as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2])]
BBC["SHL","Tjja",] <- -273.15 + bridge_ts_jja[,as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2])]



BBC["TUL","Tann",] <- -273.15 + bridge_ts[,as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2])]
BBC["TUL","Pann",] <- secs_in_a_year * bridge_precip[,as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2])]
BBC["TUL","Tdjf",] <- -273.15 + bridge_ts_djf[,as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2])]
BBC["TUL","Tjja",] <- -273.15 + bridge_ts_jja[,as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2])]



list_BBC <- list()
list_BBC[[1]] <- BBC
list_BBC[[2]] <- coord_BBC
names(list_BBC) <- c("BBC","coord_BBC")
save(list_BBC, file=paste0(projectdir,"/BBC.RData"))
```

Nils: If you want other months of temperature / precip, you can adapt the following script:
```
nc <- nc_open("/stacydata/data/BRIDGE/bbc_all_triff_rec_dyn04/teii01.temp_mm_srf.monthly.nc")
lat <- rev(ncvar_get(nc,"latitude"))
lon <- ncvar_get(nc,"longitude")
time <- ncvar_get(nc,"t")
sim_names <- paste(read.csv("/stacydata/data/BRIDGE/BRIDGE.csv",header=TRUE)$bbc_all_triff_rev_dyn04)[1:62]
sim_dates <- c(seq(120,80,by=-4),seq(78,22,by=-2),seq(21,0,by=-1))
# Mean temperature
bridge_ts <- array(0,dim=c(length(sim_dates),length(lon),length(lat)))
for (i in 1:length(sim_dates)) {
  nc <- nc_open(paste("/stacydata/data/BRIDGE/bbc_all_triff_rec_dyn04/",sim_names[i],".temp_mm_srf.monthly.nc",sep=""))
  bridge_ts[i,,] <- apply(ncvar_get(nc,"temp_mm_srf")[,length(lat):1,9601:12000],c(1,2),mean)
}
```

## FAMOUS
**FAMOUS**
Also here, temperature in Kelvin and precipitaiton in Kg/(s*m^2)
It has even the months!
(heavy 1GB)
```{r nils FAMOUS}
#Monthly:
nc <- nc_open("/stacydata/data/FAMOUS/glacial_cycle/ALL-5G-MON_3236.cdf")
famous_ts <- ncvar_get(nc,"air_temperature")-273.15 #it was Kelvin
famous_time <- ncvar_get(nc,"time")
famous_lat <- ncvar_get(nc,"latitude")
famous_lon <- ncvar_get(nc,"longitude")
nc <- nc_open("/stacydata/data/FAMOUS/glacial_cycle/ALL-5G-MON_5216.cdf")
famous_precip <- ncvar_get(nc,"precipitation_flux")*3600*24*30  #30 days per month in the simulation!
# Density of water ?
```


It's little tricky to use them (and it's a large dataset). When you import them we can check that you do it correctly (basically you have to multiply the years by 10 in the time variable, but not the months)
```{r}
summary(famous_ts)
summary(famous_precip)
```


#### Correcting the fucking time 
So the famous_time, is given as monthly means!
The year is given by whats before the dot, multiplied by 10.
Whats after the dot rapresent the month!


 c(.92, .83, .75, .67, .58, .50, .42, .33, .25, .17, .08, .00)
 correspond to January, February .... until December!
 For negative years! 
 (so year <= date < year-1 corrisponde ai punti in quell'anno)
 
 
 and c(.0014,.0847,.1681,.2514,.3347,.4181,.5014,.5847,.6681,.7514,.8347,.9181)
 same from Jan to Dic for positive years
so (year < date < year corrisponde ai punti in quell'anno se positivi)

.001388889, .0.084722222, .0.168055556, 0.251388889, 0.334722222, 0.418055556, 0.501388889, 0.584722222, .668055556, .751388889, .834722222, .918055556





Allora il tempo di famous è un po' un casino!
ma
quel che devi fare è:

- sottrarre -1 a tutti gli anni negativi
e poi
- Moltiplicare per 10 tutte le cifre PRIMA del punto.
(Le cifre dopo il punto sono i mesi!)


```{r}
famous_time_corrected <- famous_time
for (i in 1:length(famous_time)){
  if (famous_time[i]< 0){
    famous_time_corrected[i] <- famous_time[i]-1
  }
}
summary(famous_time)
summary(famous_time_corrected)

```

```{r}
length(unique(famous_time_corrected))
length(famous_time_corrected)
#rm(famous_time)
```


Now I will simply create new decimals for the camous_time, that are not periodic and doesn't fuck up all the time.

The simulation starts with December -121kyrs and ends with January 239kyrs
This means that -12001.00 is actually December of -120.000a!!!

Indeed:
```{r}
(length(famous_time_corrected)-2)%%12
```

```{r}
c_negative <- c(-0.04, rep(seq(from = -0.48, to = -0.04, by = 0.04),143928%/%12))
c_positive <- c(rep(seq(from = 0.04, to = 0.48, by =0.04),(146798-143929-1)%/%12),0.04)
new_montly_values <- c(c_negative,c_positive)
```

```{r}
timef_integer <- (abs(famous_time_corrected)%/%1)*10*sign(famous_time_corrected)

timef_integer[1:10]

mask_tmp_zero <- which(famous_time_corrected < 1 & famous_time_corrected > -2)
timef_integer[mask_tmp_zero]
timef_integer[146780:146798]

```

```{r}
timef <- timef_integer + new_montly_values

timef[1:10]
timef[mask_tmp_zero]
timef[146780:146798]
length(unique(timef))==length(timef)

round(timef[1:10])
round(timef[mask_tmp_zero])
round(timef[146780:146798])
```
Finally!!!! :D

```{r}
length(unique(timef_integer))
uni <- unique(timef_integer)

tseq <- seq(from = -120010, to = 2390, by = 10)

all(uni %in% tseq)
tseq[which(tseq %!in% uni)]
```
```{r}
famous_time_corrected[which(famous_time_corrected < -11801 & famous_time_corrected>- 11807)]
famous_time_corrected[which(famous_time_corrected < -11801 & famous_time_corrected>- 11807)]

famous_time_corrected[which(famous_time_corrected < -98 & famous_time_corrected>- 101)]

```
The values -118050 -118040 -118030 -118020 are missing from the simulation!! 
As well as the year -990 !!!!!


#### Correcting the temperature and prec

```{r}
dim(famous_lat)
dim(famous_lon)
#dim(timef)
dim(famous_time)
dim(famous_precip) #lon #lat #time
dim(famous_ts)
range(famous_lon)
range(famous_lat)

```
```{r}
dimnames(famous_ts)[[1]] <- famous_lon
dimnames(famous_ts)[[2]] <- famous_lat
#dimnames(famous_ts)[[3]] <- timef
dimnames(famous_ts)[[3]] <- famous_time

dimnames(famous_precip)[[1]] <- famous_lon
dimnames(famous_precip)[[2]] <- famous_lat
#dimnames(famous_precip)[[3]] <- timef
dimnames(famous_precip)[[3]] <- famous_time
```

```{r}
transform_longitudes <- function(lons){
    tlons <- rep(NA,length(lons))
    for (i in 1:length(lons)){
    if (lons[i] <= 180){
      tlons[i] = lons[i]
    } else {tlons[i] = lons[i]-360}
    }
    
return(tlons)}

tlon <- transform_longitudes(famous_lon)
```


```{r}
set_of_simulation_points <- as.matrix(data.frame(lon = rep(tlon, each = length(famous_lat)), lat = rep(famous_lat, length(tlon))))
sort(distGeo(set_of_simulation_points, coord_FAR[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_LIT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_MNT[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_SHL[1:2]))[1:10]
sort(distGeo(set_of_simulation_points, coord_TUL[1:2]))[1:10]
```
```{r}
sim_point_FAR <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_FAR[1:2])),],min(distGeo(set_of_simulation_points, coord_FAR[1:2]))) # same one in this case

sim_point_LIT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_LIT[1:2])),],min(distGeo(set_of_simulation_points, coord_LIT[1:2]))) # same one in this case
sim_point_MNT <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_MNT[1:2])),],min(distGeo(set_of_simulation_points, coord_MNT[1:2])))
sim_point_SHL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_SHL[1:2])),],min(distGeo(set_of_simulation_points, coord_SHL[1:2])))
sim_point_TUL <- c(set_of_simulation_points[which.min(distGeo(set_of_simulation_points, coord_TUL[1:2])),],min(distGeo(set_of_simulation_points, coord_TUL[1:2])))
```

correct the long as the model wants it
```{r}
sim_point_FAR[1] <- 120.0+180
sim_point_LIT[1] <- 120.0+180
sim_point_TUL[1] <- 82.5+180
```

Take the simulation points I am interest in 
```{r}
#c("Broadleaf tree [Tropical]", "Needleleaf tree", "C3 grass"," C4 grass", "Shrub",  "Urban", "Inland water", "Bare soil", "Ice")
# creating arrays of simulation and coordinates
FAMOUS_montly <- array(dim = c(5,2,length(famous_time)))

dimnames(FAMOUS_montly)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(FAMOUS_montly)[[2]] <- c("Tmonth","Pmonth")
dimnames(FAMOUS_montly)[[3]] <- famous_time

coord_FAMOUS_montly <- array(dim = c(5,3))

dimnames(coord_FAMOUS_montly)[[1]] <- c("FAR","LIT","MNT","SHL","TUL")
dimnames(coord_FAMOUS_montly)[[2]] <- c("Longitude","Latitude","Distance")

```



```{r}

coord_FAMOUS_montly["FAR",] <- sim_point_FAR
coord_FAMOUS_montly["LIT",] <- sim_point_LIT
coord_FAMOUS_montly["MNT",] <- sim_point_MNT
coord_FAMOUS_montly["SHL",] <- sim_point_SHL
coord_FAMOUS_montly["TUL",] <- sim_point_TUL


FAMOUS_montly["FAR","Tmonth",] <- famous_ts[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]
FAMOUS_montly["FAR","Pmonth",] <- famous_precip[as.character(sim_point_FAR[1]),as.character(sim_point_FAR[2]),]

FAMOUS_montly["LIT","Tmonth",] <- famous_ts[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]
FAMOUS_montly["LIT","Pmonth",] <- famous_precip[as.character(sim_point_LIT[1]),as.character(sim_point_LIT[2]),]

FAMOUS_montly["MNT","Tmonth",] <- famous_ts[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]
FAMOUS_montly["MNT","Pmonth",] <- famous_precip[as.character(sim_point_MNT[1]),as.character(sim_point_MNT[2]),]

FAMOUS_montly["SHL","Tmonth",] <- famous_ts[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]
FAMOUS_montly["SHL","Pmonth",] <- famous_precip[as.character(sim_point_SHL[1]),as.character(sim_point_SHL[2]),]

FAMOUS_montly["TUL","Tmonth",] <- famous_ts[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]
FAMOUS_montly["TUL","Pmonth",] <- famous_precip[as.character(sim_point_TUL[1]),as.character(sim_point_TUL[2]),]


list_FAMOUS_montly_original_time <- list()
list_FAMOUS_montly_original_time[[1]] <- FAMOUS_montly
list_FAMOUS_montly_original_time[[2]] <- coord_FAMOUS_montly
names(list_FAMOUS_montly_original_time) <- c("FAMOUS_montly","coord_FAMOUS_montly")
save(list_FAMOUS_montly_original_time, file=paste0(projectdir,"/FAMOUS_montly_original_time.RData"))
```





Do a file with the original time!

```{r}

```








####### Stuff for understanding the ultra annoying indexes
```
famous_time_corrected[1:10]
famous_time_corrected[146780:146798]

```
```
146796%%12
```
The simulation starts with December -121kyrs and ends with January 239kyrs
This means that -12001.00 is actually December of -120ka!!!
```
mask_tmp_zero <- which(famous_time_corrected < 1 & famous_time_corrected > -2)
famous_time_corrected[mask_tmp_zero]
```


Index 143930 is the first month of year zero
```
(143930 - 2)%%12
(146798-143929 -1)%%12

```
Yes the .00 at the beginnin must be relative to the previous year!

So before, when they were negative, the indexes where decreasing: January is ~ .99 Feb ~.88 etc..
while when we go to the positive sign, the indexes are positive: January is ~ .001 Feb ~.02 etc..

I will then create new indexes that round to the lower one for the negative years and round to the higher one for the positive!




```
(abs(famous_time_corrected[1:10])%/%1)*10*sign(famous_time_corrected[1:10])
(abs(famous_time_corrected[mask_tmp_zero])%/%1)*10*sign(famous_time_corrected[mask_tmp_zero])
(abs(famous_time_corrected[146780:146798]%/%1))*10*sign(famous_time_corrected[146780:146798])
```


```
(abs(famous_time_corrected[1:10])%%1)*sign(famous_time_corrected[1:10])
(abs(famous_time_corrected[mask_tmp_zero])%%1)*sign(famous_time_corrected[mask_tmp_zero])
(abs(famous_time_corrected[146780:146798]%%1))*sign(famous_time_corrected[146780:146798])


```
```
(abs(famous_time_corrected[1:10])%/%1)*10*sign(famous_time_corrected[1:10])+(abs(famous_time_corrected[1:10])%%1)*sign(famous_time_corrected[1:10]) + 0.1



(abs(famous_time_corrected[mask_tmp_zero])%/%1)*10*sign(famous_time_corrected[mask_tmp_zero])+(abs(famous_time_corrected[mask_tmp_zero])%%1)*sign(famous_time_corrected[mask_tmp_zero])

(abs(famous_time_corrected[146780:146798]%/%1))*10*sign(famous_time_corrected[146780:146798])+(abs(famous_time_corrected[146780:146798]%%1))*sign(famous_time_corrected[146780:146798])
```

```
c_negative <- c(-0.04, rep(seq(from = -0.48, to = -0.04, by = 0.04),143928%/%12))
c_positive <- c(rep(seq(from = 0.04, to = 0.48, by =0.04),(146798-143929-1)%/%12),0.04)
new_montly_values <- c(c_negative,c_positive)
```

From index 1 to index 143929 the years are negative!
```
round((abs(famous_time_corrected[1:10])%/%1)*10*sign(famous_time_corrected[1:10])+new_montly_values[1:10],0)


round((abs(famous_time_corrected[mask_tmp_zero])%/%1)*10*sign(famous_time_corrected[mask_tmp_zero]) + new_montly_values[mask_tmp_zero],0)
round((abs(famous_time_corrected[146780:146798]%/%1))*10*sign(famous_time_corrected[146780:146798])+new_montly_values[146780:146798],0)
```




NILS!
```
nc <- nc_open("/stacydata/data/FAMOUS/glacial_cycle/ALL-5G-MON_3236.cdf")
lat <- rev(ncvar_get(nc,"latitude"))
lon <- ncvar_get(nc,"longitude")
time <- ncvar_get(nc,"time")
nearby_lon <- sort(sort(abs(lon - coord_proxy[1]),index.return=TRUE)$ix[1:11])
nearby_lat <- sort(sort(abs(lat - coord_proxy[2]),index.return=TRUE)$ix[1:11])
famous_ts_tmp <- ncvar_get(nc,"air_temperature")[nearby_lon,(length(lat):1)[nearby_lat],]
nc <- nc_open("/stacydata/data/FAMOUS/glacial_cycle/ALL-5G-MON_5216.cdf")
famous_precip_tmp <- ncvar_get(nc,"precipitation_flux")[nearby_lon,(length(lat):1)[nearby_lat],]*60*60*24*30
famous_ts <- array(NA,dim=c(length(nearby_lon),length(nearby_lat),12000,12))
time_tmp <- 0
for (i in 1:146798) {
  if ((((time[i]-1/12) %/% 1) >=-12000) & (((time[i]-1/12) %/% 1) < 0)) {
    time_tmp[i] <- i
    famous_ts[,,(time[i]-1/12) %/% 1+12001,round(((time[i]-1/12) %% 1) * 12)+1] <- famous_ts_tmp[,,i]-273.15
  }
}
famous_precip <- array(NA,dim=c(length(nearby_lon),length(nearby_lat),12000,12))
time_tmp <- 0
for (i in 1:146798) {
  if ((((time[i]-1/12) %/% 1) >=-12000) & (((time[i]-1/12) %/% 1) < 0)) {
    time_tmp[i] <- i
    famous_precip[,,(time[i]-1/12) %/% 1+12001,round(((time[i]-1/12) %% 1) * 12)+1] <- famous_precip_tmp[,,i]
  }
}
gc()
```
