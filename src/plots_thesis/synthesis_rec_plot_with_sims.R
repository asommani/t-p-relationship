#### Do sinthesis plot of reconstructions AND simulation >1000yr filter

#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
# Basedrive folder definition
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive = '/Users/annarella/Dropbox'
#basedrive="/stacywork"

source(paste0(basedrive,'/sirius/src/functions/plot_fun_for_reconstructions_thesis.R'))
load(file= paste0(basedrive, "/sirius/datasets/Events/EVENTS.Rdata"))

#import data
source(paste0(basedrive,'/sirius/src/utils/read_recons_dbs.R'))
# sites
# fp <- time_restriction(foss.poll[[s]], min.year = miny,max.year = maxy)
# fcoord <- foss.meta[[s]]$coord
# mp <- modern.poll.db[[s]]
# mc <- modern.clim.db[[s]]
# dists <- seq(training_ranges[[s]][1],training_ranges[[s]][2], by=50)

colcorr <- '#f77c23'
colmi <- 'deepskyblue4'
coldiff <- 'navy'
colT <- 'goldenrod4'
colP <- '#526d7d'
colclimods <- c("#CC79A7", "#009E73",'blue2',"#D55E00" )



v <-'MI'
mods <- rep(c('WA','WAPLS','MAT'),5)
cmods <- rep( names(lclimatem_wfilt),5)
cols <-rep(c('#648FFF','#DC267F','#FFB000'),5)
clcols <- rep(colclimods,5)
siteplos <- rep(sites,each=3)
siteplossims <- rep(sites,each=4)

simTann <- array(dim = c(2,20),dimnames = list(c('mean','sd'),paste0(siteplossims,'_',cmods)))
simPann <- array(dim = c(2,20),dimnames = list(c('mean','sd'),paste0(siteplossims,'_',cmods)))

for (c in names(lclimatem_wfilt)){
    for (s in sites){
    simTann['mean',paste0(s,'_',c)] <- mean(lclimatem_wfilt[[c]][s,'Tann',,'>1000'])
    simTann['sd',paste0(s,'_',c)] <-  sd(lclimatem_wfilt[[c]][s,'Tann',,'>1000'])
    simPann['mean',paste0(s,'_',c)] <-  mean(lclimatem_wfilt[[c]][s,'Pann',,'>1000'])
    simPann['sd',paste0(s,'_',c)] <-  sd(lclimatem_wfilt[[c]][s,'Pann',,'>1000'])
  }
}


cor_arr <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
#Mi_diff <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
Mis <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
Tanns <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
Panns <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))

errPanns <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
errTanns <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))


errPannsmore <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))
errTannsmore <- array(dim = c(2,15),dimnames = list(c('mean','sd'),paste0(siteplos,'_',mods)))

md_values <- array(dim = c(2,5,4),dimnames = list(c('mean','sd'),sites, c('MI','MI_diff','Tann','Pann')))
for (s in sites){
  reconstructions_with_errors <- readRDS(file = paste0(basedrive,'/sirius/data/Processed_data/recons/',s,'_reconstructions_with_errors.rds'))
  
  for (m in c('WA','WAPLS','MAT')){
    cor_arr['mean',paste0(s,'_',m)] <- mean(lfiltar[[s]][m,'MI',,'>1000'])
    cor_arr['sd',paste0(s,'_',m)] <- sd(lfiltar[[s]][m,'MI',,'>1000'])
    
    Tanns['mean',paste0(s,'_',m)] <- mean(lfiltar[[s]][m,'Tann',,'>1000'])
    Tanns['sd',paste0(s,'_',m)] <- sd(lfiltar[[s]][m,'Tann',,'>1000'])
    
    errTanns['mean',paste0(s,'_',m)]<- mean(lfiltar[[s]][m,'Tann',,'>1000'])
    errTannsmore['mean',paste0(s,'_',m)]<- mean(lfiltar[[s]][m,'Tann',,'>1000'])
    
    if (m == 'MAT'){
    errTanns['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Tann']][['rec']]['pred',]-reconstructions_with_errors[[m]][['Tann']][['rec']]['lowval',])
    errPanns['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Pann']][['rec']]['pred',]-reconstructions_with_errors[[m]][['Pann']][['rec']]['lowval',])
    } else {
      errTanns['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Tann']][['cv']][['l']]['RMSEP',])
      errPanns['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Pann']][['cv']][['l']]['RMSEP',])
    }
    if (m == 'MAT'){
      errTannsmore['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Tann']][['rec']]['pred',]-reconstructions_with_errors[[m]][['Tann']][['rec']]['lowval',])
      errPannsmore['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Pann']][['rec']]['pred',]-reconstructions_with_errors[[m]][['Pann']][['rec']]['lowval',])
      
    } else {
      errTannsmore['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Tann']][['cv']][['h']]['RMSEP',])
      errPannsmore['sd',paste0(s,'_',m)] <- mean(reconstructions_with_errors[[m]][['Pann']][['cv']][['h']]['RMSEP',])
    }
    
    Panns['mean',paste0(s,'_',m)] <- mean(lfiltar[[s]][m,'Pann',,'>1000'])
    Panns['sd',paste0(s,'_',m)] <- sd(lfiltar[[s]][m,'Pann',,'>1000'])
    
    errPanns['mean',paste0(s,'_',m)]<- mean(lfiltar[[s]][m,'Pann',,'>1000'])
    errPannsmore['mean',paste0(s,'_',m)]<- mean(lfiltar[[s]][m,'Pann',,'>1000'])
    
    Mis['mean',paste0(s,'_',m)] <- mean(lfiltar[[s]][m,'MI_unc',,'>1000'])
    Mis['sd',paste0(s,'_',m)] <- sd(lfiltar[[s]][m,'MI_unc',,'>1000'])
    
    for (v in c('MI','Tann','Pann')){
      md_values['mean',s,v] <-foss.meta[[s]]$modern.clim.var.at.fossil.site[v,1]
      md_values['sd',s,v] <-foss.meta[[s]]$modern.clim.var.at.fossil.site[v,2]
    }
  }
}
#Mi_diff <- cor_arr-Mis


#posvect <- c(1:3,5:7,9:11,13:15,17:19)






###############PLOT
######################3
widthp <- 9
razi <- 1.414 #0.7
pdf(paste0(basedrive,"/sirius/plots/plot_thesis/synthesis_recons_with_sims_and_errors.pdf"), width = widthp, height = (widthp/razi))

leftmar <- 0
bottomar <- 0
cexi <- 1.5 #balls
cexitx <- 1.5 #lateral text
cex.bottom.tx <- 1.5 #bottom text
len.bar.arrows <- 0.06
pch.rec <- 20
pch.mod <- 1
pch.sim <-4


colerr <- adjustcolor('red3',0.3)
colerrmore <- adjustcolor('gray65',0.9)


test <- c(2,3,4,7,8,9,10,13)

rec_sim_div <- rep(5.5,5)+ c(0:4)*(test[length(test)]+1)
  
a<-1
# points
posvect <- a*c(test,test[length(test)]+1+test,  
             (test[length(test)]+1)*2+test,
             (test[length(test)]+1)*3+test,
             (test[length(test)]+1)*4+test)
# divisions
moddata <- a*c(test[length(test)]+1,
               (test[length(test)]+1)*2,(test[length(test)]+1)*3,(test[length(test)]+1)*4)

xlims<- a*c(min(posvect),max(posvect))

maskrec <- rep(1:3,5)+rep(8*c(0:4),each=3)
masksim <- rep(4:7,5)+rep(8*c(0:4),each=4)
maskmod <- rep(8,5)+rep(8*c(0:4),each=1)

posbottomtxt <- rep(6.5,5)+13*c(0:4)

par(mfrow=c(2,1),mar = c(bottomar,leftmar,0,0),cex.axis=cexi,cex.lab=cexi, oma = c(1.9,3.5,0.3,1.8))


### T corr
ylims <- range(Tanns['mean',]+Tanns['sd',],Tanns['mean',]-Tanns['sd',],simTann['mean',]+simTann['sd',],simTann['mean',]-simTann['sd',],
               md_values['mean',,'Tann']+ md_values['sd',,'Tann'], md_values['mean',,'Tann']- md_values['sd',,'Tann'])
plot(NULL,xaxt='n',ylim=ylims,xlim=xlims,xlab = '',yaxt='n')
abline(v = moddata,lty=1,col='grey')
abline(v = rec_sim_div,lty=2,col='grey85')

abline( h= seq(ceiling(ylims)[1],ceiling(ylims)[2],by=1), col='grey95', lty=2)
abline( h= seq(-10,25,by=5), col='grey85', lty=2)

axis(2, padj=0.5)
axis(4, padj=-0.2)

mtext("Tann [°C]", side = 2, line = 1.9, adj=0.5, cex=cexitx)
arrows(x0=posvect[maskrec], y0=errTannsmore['mean',]-errTannsmore['sd',],
       x1=posvect[maskrec], y1= errTannsmore['mean',]+errTannsmore['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = colerrmore)
# arrows(x0=posvect[maskrec], y0=errTanns['mean',]-errTanns['sd',],
#        x1=posvect[maskrec], y1= errTanns['mean',]+errTanns['sd',],
#        code=3, #above and below arrows heads
#        angle=0, #angle of the arrows heads (default 30)
#        length = len.bar.arrows #length of arrows heads
#        , col = colerr)
points(x=posvect[maskrec],Tanns['mean',], col=cols,cex=cexi,pch=pch.rec)
arrows(x0=posvect[maskrec], y0=Tanns['mean',]-Tanns['sd',],
       x1=posvect[maskrec], y1= Tanns['mean',]+Tanns['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = cols)



points(posvect[maskmod],md_values['mean',,'Tann'],pch=pch.mod,cex=cexi)
arrows(x0=posvect[maskmod], y0=md_values['mean',,'Tann']-md_values['sd',,'Tann'],
       x1=posvect[maskmod], y1= md_values['mean',,'Tann']+md_values['sd',,'Tann'],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = 'black')

points(posvect[masksim],simTann['mean',],pch=pch.sim,cex=cexi, col=clcols)

arrows(x0=posvect[masksim], y0=simTann['mean',]-simTann['sd',],
       x1=posvect[masksim], y1= simTann['mean',]+simTann['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = clcols)

### P
ylims <- range(Panns['mean',]+Panns['sd',],Panns['mean',]-Panns['sd',],simPann['mean',]+simPann['sd',],simPann['mean',]-simPann['sd',],
               md_values['mean',,'Pann']+ md_values['sd',,'Pann'], md_values['mean',,'Pann']- md_values['sd',,'Pann'])
plot(NULL, xaxt='n',ylim=ylims,xlim=xlims,xlab = '',yaxt='n')
abline(v = moddata,lty=1,col='grey')
abline( h= seq(0,2000,by=100), col='grey95', lty=2)
abline( h= seq(0,2000,by=500), col='grey85', lty=2)
abline(v = rec_sim_div,lty=2,col='grey85')


legend('topleft', legend=c( paste(c('WA','WAPLS','MAT'),'rec.'),paste(names(lclimatem_wfilt),'sim.'),'Present'), col=c(cols[1:3],clcols[1:4],'black'),pch=c(rep(20,3),rep(4,4),1),
       cex = 1, bty='n',bg='white',pt.cex=cexi)
axis(2, padj=0.5)
axis(4, padj=-0.2)


arrows(x0=posvect[maskrec], y0=errPannsmore['mean',]-errPannsmore['sd',],
       x1=posvect[maskrec], y1= errPannsmore['mean',]+errPannsmore['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = colerrmore)

# arrows(x0=posvect[maskrec], y0=errPanns['mean',]-errPanns['sd',],
#        x1=posvect[maskrec], y1= errPanns['mean',]+errPanns['sd',],
#        code=3, #above and below arrows heads
#        angle=0, #angle of the arrows heads (default 30)
#        length = len.bar.arrows #length of arrows heads
#        , col = colerr)


points(x=posvect[maskrec],Panns['mean',], col=cols,cex=cexi,pch=pch.rec)
mtext("Pann [mm/yr]", side = 2, line = 1.9, adj=0.5, cex=cexitx)
arrows(x0=posvect[maskrec], y0=Panns['mean',]-Panns['sd',],
       x1=posvect[maskrec], y1= Panns['mean',]+Panns['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = cols)
points(posvect[maskmod],md_values['mean',,'Pann'],pch=pch.mod,cex=cexi)
arrows(x0=posvect[maskmod], y0=md_values['mean',,'Pann']-md_values['sd',,'Pann'],
       x1=posvect[maskmod], y1= md_values['mean',,'Pann']+md_values['sd',,'Pann'],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = 'black')
points(posvect[masksim],simPann['mean',],pch=pch.sim,cex=cexi, col=clcols)
arrows(x0=posvect[masksim], y0=simPann['mean',]-simPann['sd',],
       x1=posvect[masksim], y1= simPann['mean',]+simPann['sd',],
       code=3, #above and below arrows heads
       angle=90, #angle of the arrows heads (default 30)
       length = len.bar.arrows #length of arrows heads
       , col = clcols)

axis(1, outer = T, padj = -0.3, line = -bottomar, labels = sites, at= posbottomtxt,tick=F, cex.axis=cex.bottom.tx)

dev.off()



