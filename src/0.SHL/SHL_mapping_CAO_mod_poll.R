#Plot SHL calibration sites (Cao et al.)


library(maps)
library(graphics)
library(raster)
library(rgdal) #for axes plotting
library(grDevices)

#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive="/Users/annarella/Dropbox"


# load the restricted dataset, from Kira. (4069 sites, meta.full contains also the discarted ones)


#load the original Cao one: (5192 sites)
load(paste0(basedrive,"/sirius/datasets/SHL_data/moderndata.RData"))
#climate station
load(file=paste0(basedrive,"/sirius/datasets/SHL_data/stationdata/jingyudata.RData"))
JingyuClim$loc<-c(126.8078,42.3866,549) 
SHL<-list()
SHL$loc<- c(42.283300,126.600000,797.0)

#% loads the functions mycirc and vectordestination
source(file=paste0(basedrive,"/sirius/src/SHL/Original_Kira_e_miei_appunti/functions/circlevector.R"))

Point<-SHL$loc[c(2,1)]



######################################## Compare full, and two restricted datasets
ind.z<-colSums(mod.poll)<1 #% all false, no pollen has zero counts everywere (all(colSums(mod.poll)<1)== FALSE)

# restricted dataset by MTWA
#svg(filename=paste0(basedrive,"/sirius/plots/SHL/mod_clim_Tmtwa_Jungyu.svg"),width=7,height = 5)
hist(mod.clim$Mtwa);
abline(v=JingyuClim$T.mtwa$avg,col="red");
abline(v=JingyuClim$T.mtwa$quant[c(1,3)],col="red",lty=2);
#dev.off()
#svg(filename=paste0(basedrive,"/sirius/plots/SHL/mod_clim_Pann_Jungyu.svg"),width=7,height = 5)
hist(mod.clim$Pann); 
abline(v=JingyuClim$P.ann$avg[1],col="red");
abline(v=JingyuClim$P.ann$avg[c(2,4)],col="red",lty=2);
#dev.off()

#% creates booelian vectors 
i.o1<-(mod.clim$Mtwa>23.5)
i.o2<-(mod.clim$Mtwa<10)
i.o3<-(mod.clim$Pann>1200)
# i.o4<-(mod.clim$Pann<100)

i.o4<-(mod.clim$Latitude<40)&(mod.clim$Longitude<100)

radius1<-400*1000

#svg(filename=paste0(basedrive,"/sirius/plots/SHL/characterizing_calibration_sites_Kira.svg"),width=8,height = 8)
map(xlim = c(70,185), ylim=c(15,85), interior = FALSE) #% using map! (since I don't have the function addland())
points(mod.clim$Longitude,mod.clim$Latitude,cex=0.5)
points(mod.clim$Longitude[i.o1],mod.clim$Latitude[i.o1],col=2,cex=0.5)
points(mod.clim$Longitude[i.o2],mod.clim$Latitude[i.o2],col=3,pch=2,cex=0.5)
points(mod.clim$Longitude[i.o3],mod.clim$Latitude[i.o3],col=4,pch=3,cex=0.5)
abline(h=35,v=100)
legend("bottomright",col=seq(2,5),c("Mtwa>24C","Mtwa<10","Pann>1200"),pch=c(1,2,3,4))
map.axes()
points(Point[1],Point[2],cex=2,pch=3,col="magenta",lwd=3)
lines(mycirc(Point,400*1000),col="magenta")
lines(mycirc(Point,800*1000),col="magenta")
lines(mycirc(Point,1000*1000),col="magenta")
lines(mycirc(Point,2000*1000),col="magenta")
lines(mycirc(Point,3000*1000),col="magenta")
#dev.off()


### adding land
mondo <- brick(paste0(basedrive,"/sirius/naturalearth/HYP_HR_SR_OB_DR/HYP_HR_SR_OB_DR.tif"))

#quartz()
#svg(filename=paste0(basedrive,"/sirius/plots/SHL/characterizing_calibration_sites_radius.svg",width=10.8,height = 8))
plotRGB(mondo,ext = extent(70,185,15,85), interpolate= TRUE, axes=TRUE, xlab="Longitude",ylab="Latitude")
legend(160,32,col=seq(2,5),c("Mtwa>24C","Mtwa<10","Pann>1200"),pch=c(1,2,3,4), bty="o",bg="white")
points(mod.clim$Longitude,mod.clim$Latitude,cex=0.5)
points(mod.clim$Longitude[i.o1],mod.clim$Latitude[i.o1],col=2,cex=0.5)
points(mod.clim$Longitude[i.o2],mod.clim$Latitude[i.o2],col=3,pch=2,cex=0.5)
points(mod.clim$Longitude[i.o3],mod.clim$Latitude[i.o3],col=4,pch=3,cex=0.5)
abline(h=35,v=100)
points(Point[1],Point[2],cex=2,pch=3,col="magenta",lwd=3)
lines(mycirc(Point,400*1000),col="magenta")
lines(mycirc(Point,800*1000),col="magenta")
lines(mycirc(Point,1000*1000),col="magenta")
lines(mycirc(Point,2000*1000),col="magenta")
lines(mycirc(Point,3000*1000),col="magenta")
#dev.off()


#doing a plot nice to show:
#svg(filename=paste0(basedrive,"/sirius/plots/SHL/cao_sites_radius.svg",width=10.8,height = 8))
plotRGB(mondo,ext = extent(70,185,15,85), interpolate= TRUE, axes=TRUE, xlab="Longitude",ylab="Latitude")
points(mod.clim$Longitude,mod.clim$Latitude,cex=0.5,col="gray15")
points(Point[1],Point[2],cex=2,pch=3,col="maroon4",lwd=3)
#lines(mycirc(Point,500*1000),col="maroon") #500 km
lines(mycirc(Point,1000*1000),col="maroon") #1000 km
lines(mycirc(Point,2000*1000),col="maroon") #2000 km
lines(mycirc(Point,3000*1000),col="maroon") #3000 km
lines(mycirc(Point,4000*1000),col="maroon") #4000 km
abline(h=25)
#dev.off()


max(mod.clim$Latitude)#79.45
min(mod.clim$Latitude) #18.2
max(mod.clim$Longitude)#180
min(mod.clim$Longitude)#70

#svg(filename=paste0(basedrive,"/sirius/plots/SHL/cao_sites.svg"),width=10.8,height = 8)
plotRGB(mondo,ext = extent(69,190,15,85), interpolate= TRUE, axes=TRUE, xlab="Longitude",ylab="Latitude", main="Cao et al. modern pollen dataset, Lake Sihailongwan")
points(mod.clim$Longitude,mod.clim$Latitude,cex=0.5,col="gray15")
points(Point[1],Point[2],cex=2,pch=3,col="maroon4",lwd=3)
#dev.off()

#png(filename=paste0(basedrive,"/sirius/plots/SHL/cao_sites.png"),width=1080,height = 800)
# plotRGB(mondo,ext = extent(69,190,15,85), interpolate= TRUE, axes=TRUE, xlab="Longitude",ylab="Latitude", main="Cao et al. modern pollen dataset, Lake Sihailongwan")
# points(mod.clim$Longitude,mod.clim$Latitude,cex=0.5,col="gray15")
# points(Point[1],Point[2],cex=2,pch=3,col="maroon4",lwd=3)
#dev.off()

#using the raster I got the map cutted at 180.. luckily the max longitude is exactly at 180
map(xlim = c(60,190), ylim=c(15,85), interior = FALSE) #% using map! (since I don't have the function addland())


