video.MAT <- function(basedrive, lats, lons, latrec, lonrec, numAnalogs, posid, datfr, 
    vidnam = colnames(datfr)[2]) {
    # Make animation of the locations selected as best modern analogs for a given reconstruction (datfr)
    # basedrive - main folder where animation output is to be stored
    # lats, lons - locations of the modern assemblages
    # latrec, lonrec - location of the fossil site
    # numAnalogs - number of modern analogs selected (is equal to 
    # posid
    
    tx = datfr[, 1]
    x = datfr[, 2]
    xdissim = datfr[, 3]
    mapeps = 2
    data(world2MapEnv)
    colval = "green"
    
    require(maps)
    require(mapdata)
    require(animation)
    
    
    ani.options(autobrowse = FALSE, outdir = basedrive, ani.width = 900, ani.height = 900)
    # saveVideo({
    
    
    # saveGIF(expr, movie.name = 'animation.gif', img.name = 'Rplot', convert =
    # 'convert', cmd.fun = system, clean = TRUE, ...)
    saveGIF({
        for (p in length(tx):1) {
            
            #################################################### to save screenshots for (p in 1){ # for each time point p, posid contains the
            #################################################### modern analog location ids
            #################################################### png(filename=paste(basedrive,'pollen/Modern_Analog_Locs_N',numAnalogs,'_timestep',p,'.png',sep=''),title='Modern
            #################################################### Analog location',width=600,height=800,pointsize=14)
            layout(matrix(c(1, 1, 2, 3), 2, 2, byrow = FALSE))
            par(mar = rep(1, 4))
            # map('world2Hires',xlim=c(min(lons)-mapeps,max(lons)+mapeps),ylim=c(min(lats)-mapeps,max(lats)+mapeps))
            map("world", xlim = c(min(lons) - mapeps, max(lons) + mapeps), ylim = c(min(lats) - 
                mapeps, max(lats) + mapeps))
            map.axes()
            title(sprintf("%5.0f %s", tx[p], colnames(datfr)[1]))
            
            lonp = lons[posid[, p]]  #lon location of analogs for no 1
            latp = lats[posid[, p]]
            
            points(lons, lats, col = 8, pch = 1, cex = 0.5)  # all modern analog sites
            points(lonp, latp, col = colval, pch = 15, lwd = 5, cex = 1)  # selected modern analog sites
            points(x = lonrec, y = latrec, col = 2, pch = 17, cex = 2)  # proxy site
            
            # second plot - time series
            
            par(mar = c(4, 4, 4, 1))
            plot(c(min(tx), max(tx)), c(min(x), max(x)), type = "n", xlab = "Age@Depth", 
                ylab = colnames(datfr)[2], main = "Reconstruction")
            lines(tx, x)
            points(tx[p], x[p], col = colval, pch = 15, cex= 2 )
            
            
            # third plot - quality measures
            par(mar = c(4, 4, 4, 1))
            plot(tx, xdissim, col = 1, type = "l", xlab = colnames(datfr)[1], ylab = colnames(datfr)[3], 
                main = "Quality")
            # abline(h = crit.v, lwd = 2, lty = 3, col = 'red') textloc = (max(tx, na.rm = T)
            # - (max(tx, na.rm = T) - min(tx, na.rm = T)))/50 text(textloc, crit.v,
            # round(crit.q, 3), pos = 3, col = 'red')
            points(tx[p], xdissim[p], col = colval, pch = 15, cex = 2)
            
            ########## to save screenshot - dev.off() needed dev.off()
        }  # p
        ########## to save video }, video.name = sprintf('Modern_Analog_Locs_%s.mp4',vidnam),
        ########## other.opts = '-b 400k',clean=FALSE) #saveVideo
    }, movie.name = sprintf("ModAnalogLocs_%s.gif", vidnam))  #saveGIF
    
    
    
}
