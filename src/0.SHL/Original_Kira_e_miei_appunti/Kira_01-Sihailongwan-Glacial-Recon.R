#% Anna: file I am playing around with for understanding climate reconstructions
#% comments like that will be my comments 

#' ---
#' title: "Sihailongwan Climate Reconstruction"
#' author: "Kira Rehfeld & Richard Telford"
#' date: "Feb 09, 2017"
#' ---

#' File description: Paleoclimate reconstruction for Sihailongwan lake data from
# 'Martina Stebich based on the modern surface dataset from Xianyong Cao/Ulrike
#' Herzschuh.

#' Other connected scripts: 02-Sihailongwan-Glacial-Analysis.R source(file =
#' '/home/kira/R/pollen/video.MAT.R') source() and library() statements

# DONE: Richard: Biome map; DEM regional map; 
# TBD: Checking temp reconstructions
# Questions: Why is Pann so low in the late Hol./high in hte Early Hol? Why are
# the trends for MTWA/Pann so different - check correlations modern/
# reconstructions Why is the RMSEP (performance(mod)/performance(modP)) so high?
# Is this because we have this huge gradient in hte dataset that is still so
# large? Why is BMA failing so miserably, and when [i.e., what analogs are
# missing?] What's wrong with the conditioning on MTWA in the randomTF plot? It
# doesn't do whats expected.


basedrive = "/home/kira/R/"
# Path to paleolibrary (for addland(); tbd: replace by map)
path <- paste(basedrive, "paleolibrary/src/", sep = "")
source(paste(path, "header.R", sep = ""))
setwd(paste(basedrive, "pollen/Sihailongwan/glacial_shl/", sep = ""))
outputdir <- paste(getwd(), "/output", sep = "")
sourceDir("./src/lib")
library(ppcor)
# library(analogue)
library(rioja)
library(palaeoSig)
library(maps)
library(mapdata)
library(animation)
library(zoo)
library(nest)
library(palinsol)  #insolation data
library(dplyr)
library(WriteXLS)


recname <- "SHL_Glacial"
do.video <- TRUE
do.slices <- FALSE

# Function definitions
source(file = paste("./functions/video.MAT.R"))  # best modern analog video
source(file = paste("./functions/overview_matlocs.R"))  # best modern analog video
source(file = paste("./functions/reconstruct_restricted.R"))  # best modern analog video
source(file = paste("./functions/all_diagnostics.R"))  # best modern analog video
source(file=paste("./functions/circlevector.R"))
##################################### 1 Read & Clean Data
rec.vars <- c("Tann", "Pann", "Mtco", "Mtwa")
#rec.vars <- c("Pann", "Mtwa")

fnam.mapping <- paste("./data/SHLnames.csv", sep = "")  # table for harmonization of taxa
fnam.pollen <- paste("./data/SHLpollen.csv", sep = "")  # fossil pollen

##### loading the modern/fossil data processed & saved by helpscript1
#source("helpscript1-Sihailongwan-Read-Data.R")
#save("foss.poll","mod.poll","mod.clim", "mod.poll","mod.poll.meta","mod.poll.meta.full",file="~/R/pollen/Sihailongwan/glacial_shl/data/moderndata_restr.RData")
load("./data/moderndata.RData")
#load("./data/moderndata_restr.RData")
# foss.poll mod.poll mod.poll.meta mod.clim foss.time

## For comparison and benchmarking, I've processed the data from the Station Jingyu
# > JingyuClim<-list(T.mtco=T.mtco,T.mtwa=T.mtwa,T.ann=T.ann,P.ann=P.ann,T.jja=T.jja,Pr.jja=Pr.jja)
load(file="~/R/pollen/Sihailongwan/glacial_shl/data/stationdata/jingyudata.RData")
JingyuClim$loc<-c(126.8078,42.3866) 
SHL<-list()
SHL$loc<-c(126.6,42.283)
#################################### 2 Characterizing the fossil data

relevTaxa <- which(colSums(foss.poll) > 100)

strat.plot(
  foss.poll[, relevTaxa],
  yvar = foss.time,
  scale.percent = FALSE,
  y.rev = TRUE
)
decorana(sqrt(foss.poll[,relevTaxa]))
# gradient length of first axis

pca.shl <- prcomp(x = sqrt(foss.poll[, relevTaxa]), scale = TRUE) # base
rda.shl <- rda(sqrt(foss.poll))  # pca with all taxa
cca.shl <- cca(sqrt(foss.poll)) #correspondence analysis
png(filename =paste(outputdir,"/",recname,"_ca.png",sep="") )
plot(cca.shl)
text(cca.shl, display = "species", select = colSums(foss.poll >0 )>30,col="blue") 
dev.off()
summary(cca.shl)

summary(rda.shl)
summary(pca.shl)


png(filename =paste(outputdir,"/",recname,"_screeplog.png",sep="") )
screeplot(rda.shl,bstick=TRUE)
dev.off()


#x11(width=10,height=10)
png(width=800,height=800,filename = paste(outputdir,"/",recname,"_biplots.png",sep="") )
par(mfcol = c(2, 1),mar=c(2,2,2,1))
biplot(rda.shl)  # horseshoe?
biplot(pca.shl)
dev.off()

#x11()
png(filename = paste(outputdir,"/",recname,"_pcacomponents.png",sep=""))
par(mfrow = c(3, 1))
plot(foss.time, pca.shl$x[, 1], type = "l")
plot(foss.time, pca.shl$x[, 2], type = "l")
plot(foss.time, pca.shl$x[, 3], type = "l")
dev.off()



######################## 3 Characterizing & testing transfer functions #########
colnames(mod.clim)
# pairs(mod.clim[,-c(c(1:15))])
pairs(mod.clim[, c(28, 41:43)])

# mod <- rioja::crossval(WAPLS(sqrt(mod.poll), mod.clim[["Mtwa"]]), nboot = 100)
# # leave-one-out-crossvalidation - important for WAPLS
# x11()
# screeplot(mod)
# # which number of analogues gives a good rmse (in modern climate space) and for
# # either weighted mean (with dissimilarity) or averaging (uniform)
# rioja::performance(mod)
# # diagnostics -> RMSE (variable space of climate variable, for modern analog no
# # 1-N)/R2 (0-1),Skill (percentages!)
# 
# ##
# modP <- rioja::crossval(WAPLS(sqrt(mod.poll), mod.clim$Pann), nboot = 100)  #
# # leave-one-out-crossvalidation - important for WAPLS
# screeplot(modP)
# rioja::performance(modP)
# 
# x11()
# # plot(mod, k=5, wMean=TRUE) # modeled via MAT vs. predicted variable
# plot(
#   mod,
#   npls = 3,
#   xval = TRUE,
#   xlab = "true",
#   ylab = "est",
#   resid = TRUE
# )
# # residuals of true vs. est. (modern) -- underestimates high/overestimates low
# dev.off()
# 
# ###
# pred.P <- predict(modP, foss.poll[,relevTaxa])
# pred <- predict(mod, foss.poll[,relevTaxa])
# names(pred)
# x11()
# par(mar = c(4, 4, 1, 4))
# plot(foss.time, pred$fit[, 2], type = "l")
# par(new = TRUE)
# plot(  foss.time,
#   pred.P$fit[, 2],
#   col = "red",
#   type = "l",
#   axes = FALSE,
#   xlab = "",
#   ylab = ""
# )
# 
# axis(4, col.axis = "red", col = "red")
# mtext("Precip",
#       side = 4,
#       col = "red",
#       line = 2)
# dev.off()
# predicted.temp <- pred$fit[, 2]
# predicted.precip <- pred.P$fit[, 2]
# # 
# # foss.clim <- cbind(pred$fit[, 2], pred.P$fit[, 2])
# # colnames(foss.clim) <- c("Mtwa", "Pann")
# # rownames(foss.clim) <- foss.time
# # 
# # foss.clim <- as.data.frame(foss.clim)

######################################## Compare full, and two restricted datasets
ind.z<-colSums(mod.poll)<1

# restricted dataset by MTWA
hist(mod.clim$Mtwa);
abline(v=JingyuClim$T.mtwa$avg,col="red");
abline(v=JingyuClim$T.mtwa$quant[c(1,3)],col="red",lty=2);

hist(mod.clim$Pann); 
abline(v=JingyuClim$P.ann$avg[1],col="red");
abline(v=JingyuClim$P.ann$avg[c(2,4)],col="red",lty=2);

i.o1<-(mod.clim$Mtwa>23.5)
i.o2<-(mod.clim$Mtwa<10)
i.o3<-(mod.clim$Pann>1200)
# i.o4<-(mod.clim$Pann<100)

i.o4<-(mod.clim$Latitude<40)&(mod.clim$Longitude<100)

x11()
plot(mod.clim$Longitude,mod.clim$Latitude,cex=0.5)
addland()
points(mod.clim$Longitude[i.o1],mod.clim$Latitude[i.o1],col=2,cex=0.5)
points(mod.clim$Longitude[i.o2],mod.clim$Latitude[i.o2],col=3,pch=2,cex=0.5)
points(mod.clim$Longitude[i.o3],mod.clim$Latitude[i.o3],col=4,pch=3,cex=0.5)
abline(h=35,v=100)
legend("bottomright",col=seq(2,5),c("Mtwa>24C","Mtwa<10","Pann>1200"),pch=c(1,2,3,4))


Point<-SHL$loc
radius1<-400*1000


#plot(Point[1],Point[2],xlim=c(-180,180),ylim=c(-90,90),type="n",xlab="",ylab="")
#   addland()
   points(Point[1],Point[2],cex=2,pch=3,col="magenta",lwd=3)
   lines(mycirc(Point,400*1000),col="magenta")
  lines(mycirc(Point,800*1000),col="magenta")
   lines(mycirc(Point,1000*1000),col="magenta")
   lines(mycirc(Point,2000*1000),col="magenta")
   lines(mycirc(Point,3000*1000),col="magenta")
   
   
   
dev.copy(device = png,file=paste(outputdir,"/pngs/","restriction_modern_climate.png",sep=""))
dev.off()
ind.rm.mtwa<-which(i.o1|i.o2)
ind.rm.mtwa.max<-which(i.o1)

ind.rm.mtwa.pann<-which(i.o1|i.o3)
# ind.rm.mtwa.pann.max<-which(i.o1|i.o3)
ind.rm.geo<-which(i.o4)

#plot(mod.clim$Longitude[-ind.rm.mtwa.pann],mod.clim$Latitude[-ind.rm.mtwa.pann])
out.all<-reconstruct_restricted(mod.poll, mod.clim, foss.poll, foss.time, climvars=c("Mtwa","Pann"), nboot=100)
out.mtwa<-reconstruct_restricted(mod.poll[-ind.rm.mtwa.max,], mod.clim[-ind.rm.mtwa.max,], foss.poll[,relevTaxa], foss.time, climvars=c("Mtwa","Pann"), nboot=100,nam.out=paste(outputdir,"/pngs/","filterbymtwa",sep=""))
out.mtwa.pann<-reconstruct_restricted(mod.poll[-ind.rm.mtwa.pann,], mod.clim[-ind.rm.mtwa.pann,], foss.poll[,relevTaxa], foss.time, climvars=c("Mtwa","Pann"), nboot=100,nam.out=paste(outputdir,"/pngs/","filterbymtwaandprecip",sep=""))
out.geo<-reconstruct_restricted(mod.poll[-ind.rm.geo,], mod.clim[-ind.rm.geo,], foss.poll[,relevTaxa], foss.time, climvars=c("Mtwa","Pann"), nboot=100,nam.out=paste(outputdir,"/pngs/","filterbygeo",sep=""))

# out.mtwa.pann.max<-reconstruct_restricted(mod.poll[-ind.rm.mtwa.pann.max,], mod.clim[-ind.rm.mtwa.pann.max,], foss.poll[,relevTaxa], foss.time, climvars=c("Mtwa","Pann"), nboot=100,nam.out=paste(outputdir,"/pngs/","filterbymtwaandprecipmax",sep=""))
#out.mtwa.max<-reconstruct_restricted(mod.poll[-ind.rm.mtwa.max,], mod.clim[-ind.rm.mtwa.max,], foss.poll[,relevTaxa], foss.time, climvars=c("Mtwa","Pann"), nboot=100,nam.out=paste(outputdir,"/pngs/","filterbymtwamax",sep=""))

#x11(width=10,height=10)
png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwa.png",outputdir))
plotrestrictedrecons(out.restr = out.mtwa,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim)
dev.off()
png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwa_glacial.png",outputdir))
plotrestrictedrecons(out.restr = out.mtwa,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim,xlimz=range(foss.time))
dev.off()
# png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwamax.png",outputdir))
# plotrestrictedrecons(out.restr = out.mtwa.max,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim)
# dev.off()
png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwapann.png",outputdir))
plotrestrictedrecons(out.restr = out.mtwa.pann,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim)
dev.off()

png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwapann_glacial.png",outputdir))
plotrestrictedrecons(out.restr = out.mtwa.pann,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim,xlimz = range(foss.time))
dev.off()
png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_geo.png",outputdir))
plotrestrictedrecons(out.restr = out.geo,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim)
dev.off()
png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_geo.png",outputdir))
plotrestrictedrecons(out.restr = out.geo,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim,xlimz = range(foss.time))
dev.off()
# png(width=600,height=600,filename=sprintf("%s/pngs/comparison_reconstructions_mtwapannmax.png",outputdir))
# plotrestrictedrecons(out.restr = out.mtwa.pann.max,out.all = out.all,foss.time = foss.time,JingyuClim = JingyuClim)
# dev.off()
#? How do I extract the RMSEP from the mod object?

out.all$mods$WAPLS$Mtwa$fitted.values[,3]
out.all$mods$WAPLS$Mtwa$predicted
out.all$pred[[1]]


# Restrict the adopted modern dataset by this subset
ind.rm.adopted<-ind.rm.mtwa.pann

####### 4 Run BMA and WAPLS-Reconstruction for relevant climate variables ######
# colnames(mod.clim)
#
rec.vars <- c("Tann", "Pann", "Mtco", "Mtwa")
#rec.vars<-c("Mtwa","Pann")
indrec <- sort(match(rec.vars, colnames(mod.clim)))
# colnames(mod.clim[, indrec])
nboot <- 100  # no of iterations for bootstrapping

fos <- foss.poll[,relevTaxa]  #foss.poll
age <- foss.time  #foss.poll$age
spp.orig <- mod.poll[-ind.rm.adopted,colSums(spp.orig)>1]  #mod.poll

mc.orig<-mod.clim[-ind.rm.adopted,] #mod.clim
# july temp: 20.7C Pann 775
# THE VIDEO AND RECONSTRUCTION FOR ALL VARIABLES TAKES TOO LONG--> COMMENT OUT
if (do.video) {
  source("helpscript2-Sihailongwan-RecAll-Video.R")
  save.image(file = paste(outputdir, "reconoutput_nboot",
                          nboot, ".RData", sep = ""))
}
if (do.slices) {
  source("helpscript3-Sihailongwan-ModAnalogLocs.R")
  }

# load(file='~/R/pollen/Sihailongwan/reconoutput_nboot100.RData') preds.out -
# list of BMA/WAPLS-reconstruction output


####################################### Deciding on a reconstruction
mod <- rioja::crossval(WAPLS(sqrt(spp.orig), mc.orig[["Mtwa"]]), nboot = 100)
# leave-one-out-crossvalidation - important for WAPLS
x11()
screeplot(mod)
# which number of analogues gives a good rmse (in modern climate space) and for
# either weighted mean (with dissimilarity) or averaging (uniform)
rioja::performance(mod)
# diagnostics -> RMSE (variable space of climate variable, for modern analog no
# 1-N)/R2 (0-1),Skill (percentages!)

##
modP <- rioja::crossval(WAPLS(sqrt(spp.orig), mc.orig$Pann), nboot = 100)  #
# leave-one-out-crossvalidation - important for WAPLS
screeplot(modP)
rioja::performance(modP)

x11()
# plot(mod, k=5, wMean=TRUE) # modeled via MAT vs. predicted variable
plot(
  mod,
  npls = 3,
  xval = TRUE,
  xlab = "true",
  ylab = "est",
  resid = TRUE
)
# residuals of true vs. est. (modern) -- underestimates high/overestimates low
dev.off()

###
pred.P <- predict(modP, fos)
pred <- predict(mod, fos)

x11(width=9,height=6)
par(mar = c(4, 4, 1, 4))
plot(foss.time, pred$fit[, 3], type = "l",axes=FALSE,ylab="")
arrows(0,JingyuClim$T.mtwa$quant[1],0,JingyuClim$T.mtwa$quant[3],col="black",lwd=2,code = 3,angle=90)
mtext("Mtwa", side = 2, col = "black", line = 2)
par(new = TRUE)
plot(foss.time, pred.P$fit[, 3], col = "red", type = "l", axes = FALSE, xlab = "", ylab = "")
arrows(0,JingyuClim$P.ann$avg[2],0,JingyuClim$P.ann$avg[4],col="red",lwd=2,code = 3,angle=90)

axis(4, col.axis = "red", col = "red")
axis(1);box()
grid()
mtext("Pann", side = 4, col = "red", line = 2)
dev.copy(device = png,file=paste(outputdir,"/pngs/",recname,"_mtwa_and_pann.png",sep=""))

dev.off()


predicted.temp <- pred$fit[, 3]
predicted.precip <- pred.P$fit[, 3]
# 
foss.clim <- cbind(pred$fit[, 3], pred.P$fit[, 3])
colnames(foss.clim) <- c("Mtwa", "Pann")
rownames(foss.clim) <- foss.time
# 
foss.clim <- as.data.frame(foss.clim)



###################################### 5 Running the diagnostics ###############

# <-------------------- take out unreasonable climate variables (only MTwa/Pann)

####### TBD: Change file names below... to map to new git repository
mod.poll<-mod.poll[-ind.rm.adopted,]
mod.clim<-mod.clim[-ind.rm.adopted,]
mod.poll<-mod.poll[,colSums(mod.poll)>1]
rtfWAPLS <-
  randomTF(
    mod.poll,
    mod.clim[, indrec],
    foss.poll[,relevTaxa],
    fun = WAPLS,
    col = 3,
    n = 1000
  )
png(filename = sprintf("%s/randomTF_WAPLS_%s.png", outputdir, recname),
  width = 900, height = 400, res = 100 )
plot(rtfWAPLS, p.val = 0.9, cex = 1.2)
dev.off()



# var.i<-c(1,2,3,6) <------------------------ conditioning with bugfix
rtfc <-
  randomTF(
    mod.poll,
    mod.clim[indrec],
    foss.poll[,relevTaxa],
    fun = WAPLS,
    col = 3,
    n
    = 999
  )  #checking for the variance that is beyond MTwa
rtfc.P <-
  randomTF(
    mod.poll,
    mod.clim[indrec],
    foss.poll[,relevTaxa],
    fun = WAPLS,
    col = 3,
    n = 999,
    condition = data.frame(foss.clim$Mtwa)
  )
#checking for the variance that is beyond Pann
rtfc.T <-
  randomTF(
    mod.poll,
    mod.clim[indrec],
    foss.poll[,relevTaxa],
    fun = WAPLS,
    col = 3,
    n = 999,
    condition = data.frame(foss.clim$Pann)
  )
#checking for the variance that is beyond Pann
x11()
par(mfcol = c(3, 1))
plot(rtfc, p.val = c(0.9))
plot(rtfc.P, p.val = c(0.9));mtext(side=3,"Cond. on mtwa") # why is MTWA still showing up???? Something is wrong!
plot(rtfc.T, p.val = c(0.9));mtext(side=3,"Cond. on Pann")
dev.copy(device=png,file=paste(outputdir,"/pngs/",recname,"randomTF.png",sep=""))
dev.off()

# dashed line -> first eof variance (overall) red dotted line 95% sign. wrt white
# noise solid lines -> prop of variance explained in fossil record by
# reconstruction in the variable Assumption of white noise for climate variable
# for gray background what's the steps that lead to this gray plot? 1) simulate
# white noise climate variable 2) select nearest neighbors according to taxa (so
# they are the same for all different clim. realiz) 3) use MAT to get weighted
# mean of climate of modern sites as estimate for climate in the past of this
# sample (downcore) 5) 'constrained by the reconstruction - variance of the taxa
# Check what the output looks like in cca.r Get this for temperature and
# precipitation Get total variance explained by reconstruction in the fossil
# pollen from CCA Get unique variance (partial CCA) by partialling out the other
# variable

# site <- foss.poll#cal.set[[ind]] env.site <- mod.clim[c(8,11)];
# #cbind(site$temperature,site$precipitation) poll.site<-mod.poll[,-c(1,111)] ##
# CCA cca.local<- cca(sqrt(poll.site[,
# colSums(sqrt(poll.site))>0.1])~mod.clim[8]+mod.clim[11], data = env.site,scale
# = TRUE) #+ annual.mean.tt+ annual.mean.pp plot(cca.local,type='p')
# ordisurf(cca.local~env.site$MTWA,add = TRUE) # add ordination surface of
# warmest month temperature
#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
# #partial CCAs # pcca <- sapply(1:length(cal.set),function(zz) { # site <-
# cal.set[[zz]] modern variance explained - unique
env.site <-
  mod.clim[, indrec]  #cbind(site$temperature,site$precipitation)
# spec.site<-mod.poll
mod.pcca.mtwa <-
  cca(sqrt(mod.poll) ~
        env.site[, 1] + Condition(as.matrix(env.site[,-1])), scale = TRUE)
round(mod.pcca.mtwa$CCA$eig / mod.pcca.mtwa$tot.chi, 3)

mod.pcca.pann <-
  cca(sqrt(mod.poll[, colSums(sqrt(mod.poll)) > 0.1]) ~
        env.site[, 2] + Condition(as.matrix(env.site[,-2])), scale = TRUE)
round(mod.pcca.pann$CCA$eig / mod.pcca.pann$tot.chi, 3)

## modern variance total

x <- 1
mod.cca.var.mtwa <-
  cca(sqrt(mod.poll[, colSums(sqrt(mod.poll)) > 0.1]) ~
        env.site[, x], scale = TRUE)
round(mod.cca.var.mtwa$CCA$eig / mod.cca.var.mtwa$tot.chi, 3)
x <- 2
mod.cca.var.pann <-
  cca(sqrt(mod.poll[, colSums(sqrt(mod.poll)) > 0.1]) ~
        env.site[, x], scale = TRUE)
round(mod.cca.var.pann$CCA$eig / mod.cca.var.pann$tot.chi, 3)


## fossil variance explained - unique env.site <-
## mod.clim[,indrec]#cbind(site$temperature,site$precipitation)
spec.site <- mod.poll
foss.pcca <- c()

foss.pcca.mtwa <-
  cca(sqrt(foss.poll[, colSums(sqrt(foss.poll)) > 0.1]) ~ foss.clim[,
                                                                    1] + Condition(as.matrix(foss.clim[,-1])), scale = TRUE)
foss.pcca[1] <-
  round(foss.pcca.mtwa$CCA$eig / foss.pcca.mtwa$tot.chi, 3)

foss.pcca.pann <-
  cca(sqrt(foss.poll[, colSums(sqrt(foss.poll)) > 0.1]) ~ foss.clim[,
                                                                    2] + Condition(as.matrix(foss.clim[,-2])), scale = TRUE)
foss.pcca[2] <-
  round(foss.pcca.pann$CCA$eig / foss.pcca.pann$tot.chi, 3)
names(foss.pcca) <- c("MTWA", "PANN")

## fossil variance total
foss.cca <- c()
x <- 1
foss.cca.var.mtwa <-
  cca(sqrt(foss.poll[, colSums(sqrt(foss.poll)) > 0.1]) ~ foss.clim[,
                                                                    x], scale = TRUE)
foss.cca[x] <-
  round(foss.cca.var.mtwa$CCA$eig / foss.cca.var.mtwa$tot.chi, 3)
x <- 2
foss.cca.var.pann <-
  cca(sqrt(foss.poll[, colSums(sqrt(foss.poll)) > 0.1]) ~ foss.clim[,
                                                                    x], scale = TRUE)
foss.cca[x] <-
  round(foss.cca.var.pann$CCA$eig / foss.cca.var.pann$tot.chi, 3)

x11(width = 7, height = 4)
par(font.axis = 2, font.lab = 2)
barplot(foss.cca[order(foss.cca, decreasing = TRUE)] * 100)
barplot(foss.pcca[order(foss.cca, decreasing = TRUE)] * 100, add = TRUE, col = 1)
mtext(side = 2,
      "%",
      line = 2.2,
      font = 2)
title(main = "Variance explained by reconstruction in fossil record (CCA)")
dev.copy2pdf(
  file =
    paste(outputdir, "/", recname, "varexplained_fossil.pdf", sep = ""),
  out.type = "pdf"
)
dev.off()



############################################################### Misc



###################### READING Griffiths 2013 (Flores Stalagmite) <-> northward shift of the ITCZ/
###################### drying of the Australasian monsoon, suggest strengthening of hte EASM due to
###################### northward shift <-> can we see this?  Wang 2008 (Hulu?)  Some more reading
###################### Pausata 2011: d18O response in Chinese stalagmites reflect ISM, not ASM changes
###################### Sun 2012 '...srecords of grain size variations from the northwestern Chinese
###################### Loess Plateau, dated using optically stimulated luminescence. We reconstruct
###################### changes in the strength of the East Asian winter monsoon over the past 60,000
###################### years and find reconstructed millennial-scale variations that are broadly
###################### correlated with temperature variations over Greenland, suggesting a common
###################### forcing.'  -> general correlation between EASM and Greenland is not surprising.
###################### Yancheva 2007 P wang 200 Evolution and variability of the Asian monsoon system:
###################### state of the art and outstanding issues -> check for open questions

# Rohling 2009 :Controls on the East Asian monsoon during the last glacial cycle,
# based on comparison between Hulu Cave and polar ice-core records -> this one I
# should really read

# High frequency pulses of East Asian monsoon climate in the last two
# glaciations: link with the North Atlantic Z Guo, T Liu, J Guiot, N Wu, H Lü, J
# Han, J Liu, Z Gu - Climate Dynamics, 1996 - Springer

# Catastrophic drought in the Afro-Asian monsoon region during Heinrich event 1
# JC Stager, DB Ryves, BM Chase, FSR Pausata - Science, 2011 - sciencemag.org

# North Atlantic forcing of millennial-scale Indo-Australian monsoon dynamics
# during the Last Glacial period RF Denniston, KH Wyrwoll, Y Asmerom… -
# Quaternary Science …, 2013 - Elsevier

# Also review some model papers on the Glacial monsoon (or get some LGM/midHol
# CMIP runs?)  Crucifix and Hewitt 2005! Impact of vegetation changes on the
# dynamics of the atmosphere at the Last Glacial Maximum

# Last glacial maximum East Asian monsoon: Results of PMIP simulations D Jiang, X
# Lang - Journal of Climate, 2010 - journals.ametsoc.org What characteristics
# could I look at in the LGM simulations? Precip, temp? Wind directions?

# http://www.pik-potsdam.de/~stefan/sampleimages.html interesting sketches of the
# THC changes between Heinrich-Events, 'normal' Glacial states and D/O events,
# Ganopolski/Rahmstorf explain this by stochastic resonance
