library(palinsol)
library(rlist)
library(zoo)

#standard insolation function from palinsol package
insolation <- function(times, astrosol=ber78, long=pi/2, lat=65*pi/180){sapply(times, function(tt) Insol(orbit=astrosol(tt), long=long, lat=lat))}

#adapted i.e. taken from https://bitbucket.org/mcrucifix/insol
Insol_H <- function(orbit,long, lat=65*pi/180,S0=1365, H=NULL)
{
  # Returns daily mean or hourly incoming solar insolation after Berger (1978) as function of the hour angle of the sund
  
  # eps  : obliquity (radians)
  # varpi: longitude of perihelion (radians)
  # e : eccentricity 
  # long : true solar longitude 
  #        (radians; = pi/2 for summer solstice )
  #                     pi   for autumn equinox  )
  #                     3* pi/2 for winter solstice )
  #                     0 for spring equinox )
  # lat : latitude
  # orbit is a list containing eps, varpi and e (see Ber78)
  # S0 : Total solar irradiance (default : 1365 W/m^2)
  # returns : daily mean incoming solar radiation at the top of the atmosphere
  #           in the same unit as S0. 
  # H : if NULL (default): compute daily mean insolation
  # else: hour angle (in radians) at which insolation is being computed
  
  varpi <- NULL
  eps <- NULL
  ecc <- NULL
  for (i in names(orbit)) assign(i,orbit[[i]])
  nu <- long - varpi
  rho <- (1-ecc^2)/(1+ecc*cos(nu))
  sindelta <- sin(eps)*sin(long)
  cosdelta <- sqrt(1-sindelta^2)
  sinlatsindelta <- sin(lat)*sindelta
  coslatcosdelta <- cos(lat)*cosdelta 
  if (is.null(H))
  {
    cosH0 <- min(max(-1,-sinlatsindelta/coslatcosdelta),1)
    sinH0 <- sqrt(1-cosH0^2)
    H0 <- acos(cosH0)
    print(H0)
    insol <- S0/(pi*rho^2)*(H0*sinlatsindelta+coslatcosdelta*sinH0)
  }
  else 
  {
    insol <- pmax(0, S0/(rho^2)*(sinlatsindelta+coslatcosdelta*cos(H)))
  }
  return(insol)
}

insolation_year <- function(times, astrosol=la04, long=pi/2, lat=65*pi/180){sapply(times, function(tt) Insol_H(orbit=astrosol(tt), long=long, lat=lat,  H=NULL))}

insolation_day <- function(times, astrosol=la04, lat=65*pi/180)
{ 
  seqs <- seq(from=1, to=360, by=1)
  dailyTSI <- lapply(times, function(tt){Insol(astrosol(tt), long = day2l(astrosol(tt), seqs), lat = lat)})
  dailyTSI.mat <-list.rbind(dailyTSI)
  vec <- as.vector(t(dailyTSI.mat))
  dat <-zoo(vec, order.by = seq(from = times[1], to = times[length(times)]+1, by = 1/360)[-1])
  return(dat)
}

insolation_hour <- function(times, astrosol=la04, tmin=0.5, lat=65*pi/180)
{
  hourly_insolation <- function(tt, astrosol=la04, tmin, lat = 65*pi/180){
    hours <- seq(from=-12, to=12-tmin, by=tmin)
    hourlyTSI <- lapply(seq(from = 1, to = 360, by = 1), function(d){Insol_H(astrosol(tt), long = day2l(astrosol(tt), d), lat = lat, H=hours*pi/12)})
    hourlyTSI.mat <-list.rbind(hourlyTSI)
    vec <- as.vector(t(hourlyTSI.mat))
    dat <-zoo(vec, order.by =seq(from = tt, to = tt+1, by = 1/360*(diff(hours)[1]/24))[-1])
    return(dat)}
  years_list <- lapply(times, function(tt) hourly_insolation(tt, astrosol, tmin=tmin, lat=lat))
  list.rbind(years_list)
}


