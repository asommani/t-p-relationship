### script of video on MAT

library(rioja)
library(palaeoSig)
library(sp)
library(gstat)
library(tidyverse)
library(magick)
library(maps)

h_cv <- 678.5
basedrive = '/Users/annarella/Dropbox'
source(paste0(basedrive,'/sirius/src/functions/fp_mc_mf_subset_all_sq_dist.R'))
source(paste0(basedrive,'/sirius/src/functions/Cvs_models.R'))
source(paste0(basedrive,'/sirius/src/functions/analogs.R'))

load(paste0(basedrive, "/sirius/datasets/SHL_Cao_reconstructions/SHL_moderndata.RData"))

fcoord <- coord_SHL <- c(126.6,42.2833,797)
#### checking size of errors and closest analogues
# the MAT error also depends on the training set size!!!!
#### IN MAT the deta shouldn't be square rooted!!

mdata <- space_restriction_on_mp_and_mc(mp= mod.poll,mc=mod.clim, fcoord  = fcoord, dist=20000,abovelat = 25) #aall of the sites
geodist <- fields::rdist.earth(select(mdata$mc, Longitude, Latitude), miles = FALSE)
mod_l <- crossval(MAT(mdata$mp, mdata$mc[,'Tann'],lean=F,k=10), cv.method='lgo')
mod_h <- crossval(MAT(mdata$mp, mdata$mc[,'Tann'],lean=F,k=10), cv.method = "h-block", h.dist = geodist, h.cutoff = h_cv)
performance(mod_l)$crossval[20]
performance(mod_h)$crossval[20] 
matmodel <- mod_h
matpred <- predict(mod_h,foss.poll , k=10, sse=FALSE, lean=F)
mod_h_prec <- crossval(MAT(mdata$mp, mdata$mc[,'Pann'],lean=F,k=10), cv.method = "h-block", h.dist = geodist, h.cutoff = h_cv)
performance(mod_h_prec)$crossval[20] 

medians <- analogues_median(matpred)
#mpquantiles <- quantile(ds, c(.05,.1,.2)) 

rmsep <- performance(mod_l)$crossval[20]
rmsep <- performance(mod_h)$crossval[20] # the h-block for mat is very pessimistic
errs <- rmsep/(1-medians)

coords <- analogues_coordinates(matpred,mod.clim)
names(medians)<- dimnames(coords)[[1]]


######### Do the video, it takes some time
# Imput variables: coords, fcoord,basedrive,mdata
for (t in dimnames(coords)[[1]]){
png(paste0(basedrive,'/sirius/plots/Thesis_ultimate/gifs/SHL/',t,'.png'),width = 700, height = 660)
map(xlim = c(70,185), ylim=c(15,85), interior = FALSE) #% using map! (since I don't have the function addland())
points(mdata$mc$Longitude,mdata$mc$Latitude,cex=0.5)
points(fcoord[1],fcoord[2],cex=2,pch=3,col="green",lwd=3)
points(coords[t,,1],coords[t,,2],cex=1.5, col= 'red', pch=16)
text(165,20,paste0(t,' ka'), cex=2)
text(164,16,paste0('M=',round(medians[t],2)), cex=1.4)
map.axes()
dev.off()
}
imgs <- paste0(basedrive,'/sirius/plots/Thesis_ultimate/gifs/SHL/',dimnames(coords)[[1]],'.png')
img_list <- lapply(imgs, image_read)
## join the images together
img_joined <- image_join(img_list)
## animate at 2 frames per second
img_animated <- image_animate(img_joined, fps = 2)
image_write(image = img_animated,
            path = paste0(basedrive,'/sirius/plots/Thesis_ultimate/gifs/SHL/SHL.gif'))


##########

### have a look of errors in the WAPLS
# besthbl <- return_wapls_comp_to_use_and_cv_evaluation('h-block',Tann_SHL_CV_WAPLS)
# bestloo <- return_wapls_comp_to_use_and_cv_evaluation('loo',Tann_SHL_CV_WAPLS)
# plot_wapls_cv_outputs(besthbl = besthbl,bestloo=bestloo)
# distances_for_pred <- as.character(seq(1000,3500,by=50))
# vals <- Tann_SHL_CV_WAPLS
# cvtype <- 'h-block'
# predictions <- array(dim= c(length(distances_for_pred),length(rownames(foss.poll))), dimnames = list(distances_for_pred, rownames(foss.poll)))
# for (d in distances_for_pred){
#   comp <- besthbl['comp',d]
#   predictions[d,] <- predict(vals[[cvtype]][[d]], sqrt(foss.poll))$fit[,comp]
# }
# summary(predictions[,1])
# ranges <- rep(NA, length(predictions[2]))
# for (i in seq(dim(predictions)[2])){
# ranges[i] <- summary(predictions[,i])[5]-summary(predictions[,i])[2]
# }
# plot(foss.time,ranges)
# mean(ranges)




mdata <- space_restriction_on_mp_and_mc(mp= mod.poll,mc=mod.clim, fcoord  = fcoord, dist=1000,abovelat = 25) #aall of the sites
geodist <- fields::rdist.earth(select(mdata$mc, Longitude, Latitude), miles = FALSE)
mod_l <- crossval(MAT(mdata$mp, mdata$mc[,'Tann'],lean=F,k=10), cv.method='lgo')
mod_h <- crossval(MAT(mdata$mp, mdata$mc[,'Tann'],lean=F,k=10), cv.method = "h-block", h.dist = geodist, h.cutoff = h_cv)
