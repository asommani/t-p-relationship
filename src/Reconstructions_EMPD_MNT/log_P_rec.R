############
#### Do reconstructions with log(P)

# Basedrvie 
#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive = '/Users/annarella/Dropbox'

# Libraries
library(scales)
library(rioja)

# data loading
load(paste0(basedrive, "/sirius/datasets/MNT-EMPD_data_for_reconstructions/MNT_moderndata.RData"))
load(paste0(basedrive,'/sirius/datasets/MNT-EMPD_data_for_reconstructions/MNT_modernclimate.Rdata'))
source(file=paste0(basedrive,'/sirius/scr/functions/plot_mont_rec_components.R'))
load(paste0(basedrive,'/sirius/datasets/MNT-EMPD_data_for_reconstructions/saved_WAPLS_models/fullclimvariables_reconstrucions_all_sites_first200taxa_Related.Rdata'))
rm(tests_list,reconstructions)
array_components_linear <- array_components
rm(array_components)

load(paste0(basedrive,'/sirius/datasets/MNT-EMPD_data_for_reconstructions/saved_WAPLS_models/logP_models_EMPD.Rdata'))
model00001 <-models

load(paste0(basedrive,'/sirius/temp/logP_models_EMPD2.Rdata'))
model1 <- models


#################### 
## FUNCTIONS
Ttests_loops_ans_skill_table <- function(modelslist, ncomp= 5){
  tests_list <- list()
  for (i in c(1:length(models))){ tests_list[[i]] <- rand.t.test(models[[i]]) }
  names(tests_list) <- names(modelslist)
  table_of_skills <- matrix(nrow = length(modelslist), ncol = ncomp, dimnames = list(names(modelslist),names(tests_list[[1]][,'Skill']) ))
  for (i in c(1:length(modelslist))){
    table_of_skills[i,] <- round(tests_list[[i]][,'Skill'],1)
  }
  return(list(tests_list = tests_list, table_of_skills = table_of_skills))
}

predict_Rioja_models <- function(models,modern.pollen){
reconstructions <- list()
 for (i in c(1:length(models))){
   reconstructions[[i]] <- predict(models[[i]],MNT_prc)
   reconstructions[[i]]$fit <- exp(reconstructions[[i]]$fit)
 }
 names(reconstructions) <- names(models)
 
 array_components <- array(dim = c(542, length(reconstructions), 5))
 for (i in c(1:length(reconstructions))){
   for (j in c(1:5)){
     array_components[,i,j] <- reconstructions[[i]]$fit[,j]
   }
 }
 dimnames(array_components)<-list(rownames(MNT_prc),names(reconstructions),c('1st','2nd','3rd','4th','5th'))
 
 return(list(reconstructions = reconstructions,array_components = array_components))
 }

# rename_clim_var <- function(names){
#   if (substr(names,1,1)=="p"){
#     renamed <- paste0('P',substr(names,3,5))
#   } else if (substr(names,1,1) =='t'){
#     renamed <- paste0('T',substr(names,3,5))
#   }
#   return(renamed)
#}
Pdifferences <-function(array_components, plot = TRUE,name_kind = ''){
  options(scipen=999)
  if(plot){pdf(paste0(basedrive,"/sirius/temp/plots/Pdifferences_between_mean_values_",name_kind ,".pdf"),width = 9, height  = 5.2)}
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(13:16),1],1,sum)-array_components[,17,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Pseasons - Pann",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(1:12),1],1,sum)-array_components[,17,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Pmonths - Pann",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  
  
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(1,2,12),1],1,sum)-array_components[,13,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Pwintermonths - Pwinter",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  
  
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(3:5),1],1,sum)-array_components[,14,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Pspringmonths - Pspring",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  
  
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(6:8),1],1,sum)-array_components[,15,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Psummermonths - Psummer",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  
  
  plot(x=dimnames(array_components)[[1]],
       y=apply(array_components[,c(9:11),1],1,sum)-array_components[,16,1],
       type="l",
       xlim = c(100000,10000),
       main = "Difference  Pautumnmonths - Pautumn",
       xlab = 'a BP',
       ylab = 'Delta precipitation [mm]')
  if(plot){dev.off()}
}


#######################################
models <- model1
tests_list <- Ttests_loops_ans_skill_table(models)[[1]]

all_pollen_reconstructions <- predict_Rioja_models(models,MNT_prc)
relevTaxa_reconstructions <- predict_Rioja_models(models,MNT_prc[,colSums(MNT_prc)>100])

quartz()
par(mfrow=c(1,1))

reconstructions <- all_pollen_reconstructions[[1]]
array_components <- all_pollen_reconstructions[[2]]


#pdf(paste0(basedrive,"/sirius/temp/plots/lists_of_P_logtrnsformed2.pdf"),width = 9, height  = 5.2) 
for (i in c(1:length(reconstructions))){
  tests <- tests_list[[i]]
  #print((tests[,c('RMSE','Skill','p')]))
  clim_var <- rename_clim_var(names(reconstructions)[i])
  plot_components(reconstructions[[i]],type=clim_var,title = NULL)
  string <- paste('             1st, 2nd, 3rd, 4th, 5th \n','RMSE   ',
                  paste(round(tests[,'RMSE'],2),collapse =' '),'\nSkill',paste(round(tests[,'Skill'],0),collapse =' '),'\n p', paste(tests[,'p'],collapse =' '))
  title(main=string, cex.main =0.6 )
  title(main = clim_var, cex.main = 1, adj=0)
}
#dev.off()



######################
###### Analising EMPD precip distribution  
EMPD_prec <- EMPD_climate_meta[,c(19:35)]
#EMPD_prec[EMPD_prec==0] <- 1
## Log values!
EMPD_prec <- log(EMPD_prec)
hist((EMPD_prec$p_jja),breaks=100)
#####################
options(scipen=999)


array_components_e000001 <- predict_Rioja_models(model00001,MNT_prc)[[2]]
array_components_e1 <- predict_Rioja_models(model1,MNT_prc)[[2]]


Pdifferences(array_components_linear[,c(18:34),],name_kind = 'untransformed')
Pdifferences(array_components_e1,name_kind = 'e1')
Pdifferences(array_components_e000001,name_kind = 'e=000001')
########
# removing point with P negative
# 27160 ka ago!

#Weird point to remove:
"27160"

#Points to remove because Equisetum fucks up everything :
View(MNT_prc[c("30795","31174","32210","32675"),c("Equisetum","Brassicaceae")])

View(MNT_prc[time_secion_mask,colSums(MNT_prc[time_secion_mask,])>30])
time_secion_mask <- as.numeric(rownames(MNT_prc))>26000 & as.numeric(rownames(MNT_prc))<29000
