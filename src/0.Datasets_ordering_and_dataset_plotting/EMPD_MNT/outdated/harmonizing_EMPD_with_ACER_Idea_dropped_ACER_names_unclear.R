####### Main script for harmonizing EMPD pollen with ACER pollen
#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive = '/Users/annarella/Dropbox'


#meta table data with all the EMPD sites with climate data and reliability
load(file=paste0(basedrive,'/sirius/datasets/EMPD_data/EMPD-data_V2/EMPD_all_good.Rdata')) 

# taxa names accepted in the raw first vertion of the EMPD pollen matrix
load(file=paste0(basedrive,'/sirius/datasets/EMPD_data/EMPD-data_V2/p_vars_selected_taxa.Rdata'))

# EMPD pollen matrix - raw first version of penrcentages
load(file=paste0(basedrive,'/sirius/datasets/EMPD_data/EMPD-data_V2/EMPD_Pollen_raw_percentages.Rdata'))

#all(colnames(EMPD_Pollen_raw_percentages) %in% p_vars_selected_taxa$acc_varname)

Taxa_harmonized_ACER <- read.csv(file = paste0(basedrive,'/sirius/datasets/ACER_data/Taxa_harmonised/Taxa_harmonised.csv'), sep = '\t', header = TRUE)

ACER_taxa_names <- unique(as.vector(Taxa_harmonized_ACER$ACER.revised.names))
taxa_names_EMPD_unique_untouched <- sort(unique(as.vector(p_vars_selected_taxa$acc_varname)))
if(taxa_names_EMPD_unique_untouched[1] == "*sum"){
taxa_names_EMPD_unique_untouched <- taxa_names_EMPD_unique_untouched[-1]
}
###########
## Load ACER fossil datasets, for checking the important taxa names 
###########
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/MNT_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/TUL_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/LIT_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/FAR_fossil.Rdata"))
load("/Users/annarella/Dropbox/sirius/datasets/SHL_data/moderndata.Rdata")

rm(MNT_foss.poll.cnt,MNT_foss.time)
rm(LIT_foss.poll.cnt,LIT_foss.time)
rm(TUL_foss.poll.cnt,TUL_foss.time)
rm(FAR_foss.poll.cnt,FAR_foss.time)
rm(foss.time,mod.clim,mod.poll.meta)
#################
MNT_names <- colnames(MNT_foss.poll.prc)
LIT_names <- colnames(LIT_foss.poll.prc)
FAR_names <- colnames(FAR_foss.poll.prc)
TUL_names <- colnames(TUL_foss.poll.prc)

all_ACER_fossil_names <- c(MNT_names, LIT_names, FAR_names, TUL_names)
all_ACER_fossil_names <- unique(all_ACER_fossil_names)


FAR_Significant_names <- colnames(FAR_foss.poll.prc[colSums(FAR_foss.poll.prc) > 1])
MNT_Significant_names <- colnames(MNT_foss.poll.prc[colSums(MNT_foss.poll.prc) > 1])
LIT_Significant_names <- colnames(LIT_foss.poll.prc[colSums(LIT_foss.poll.prc) > 1])
TUL_Significant_names <- colnames(TUL_foss.poll.prc[colSums(TUL_foss.poll.prc) > 1])

all_ACER_significant_fossil_names <- c(FAR_Significant_names,MNT_Significant_names,LIT_Significant_names,TUL_Significant_names)
all_ACER_significant_fossil_names <- unique(all_ACER_significant_fossil_names) 

'%!in%' <- function(x,y)!('%in%'(x,y))
insignificant_names <- all_ACER_fossil_names[all_ACER_fossil_names %!in% all_ACER_significant_fossil_names]

for(i in c(1:length(insignificant_names))){
  newname <- paste0(insignificant_names[i],' *')
  insignificant_names[i] <- newname 
}

all_ACER_fossil_names_insignificant_with_asterix <- sort(c(all_ACER_significant_fossil_names,insignificant_names))

rm(MNT_names,LIT_names,FAR_names,TUL_names)
rm(MNT_Significant_names,LIT_Significant_names,FAR_Significant_names,TUL_Significant_names)
rm(MNT_foss.poll.prc,LIT_foss.poll.prc,FAR_foss.poll.prc,TUL_foss.poll.prc)


SHL_names <- colnames(foss.poll)
Caos_names <- colnames(mod.poll)
SHL_names[which(SHL_names %!in% Caos_names)] #Cistaceae is absent in the cao's dataset, 

library(analogue)
data(Pollen)
NAMPD_Pollen <- colnames(Pollen)[-1]

table_of_names <- data.frame(EMPD_taxae = c(taxa_names_EMPD_unique_untouched,rep(' ', length(ACER_taxa_names)-length(taxa_names_EMPD_unique_untouched))),
                             ACER_fossil_names_we_care_for = c(all_ACER_fossil_names_insignificant_with_asterix, rep(' ',length(ACER_taxa_names)-length(all_ACER_fossil_names_insignificant_with_asterix))),
                             NAMPD_taxae = c(NAMPD_Pollen,rep(' ', length(ACER_taxa_names)-length(NAMPD_Pollen))),
                             Caos_taxae = c(Caos_names,rep(' ', length(ACER_taxa_names)-length(Caos_names))),
                             ACER_all_taxae = ACER_taxa_names)




#########################################################
##### Essentially however I care for MNT to be compatible with EMPD, and SHL to be compatible with EMPD and Caos.
########################################################

EMPD_Pollen_raw_percentages[]

EMPD_sub_range <- EMPD_Pollen_raw_percentages[which(rownames(EMPD_Pollen_raw_percentages) %in% EMPD_all_in_range$SampleName),]
sum(colSums(EMPD_sub_range) == 0)

sum(colSums(EMPD_Pollen_raw_percentages)==0)





  