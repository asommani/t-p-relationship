#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive = '/Users/annarella/Dropbox'

load(paste0(basedrive,"/sirius/datasets/ACER_data/MT_sites.Rda"))


load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/SHL_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/MNT_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/TUL_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/LIT_fossil.Rdata"))
load(paste0(basedrive,"/sirius/datasets/MT_fossil_data/FAR_fossil.Rdata"))

rm(MNT_foss.poll.cnt,MNT_foss.time)
rm(LIT_foss.poll.cnt,LIT_foss.time)
rm(TUL_foss.poll.cnt,TUL_foss.time)
rm(FAR_foss.poll.cnt,FAR_foss.time)

rm(MNT_foss.poll.cnt,MNT_foss.time, MNT_foss.poll.prc)
rm(LIT_foss.poll.cnt,LIT_foss.time, LIT_foss.poll.prc)
rm(TUL_foss.poll.cnt,TUL_foss.time, TUL_foss.poll.prc)
rm(FAR_foss.poll.cnt,FAR_foss.time, FAR_foss.poll.prc)

load("/Users/annarella/Dropbox/sirius/datasets/SHL_data/moderndata.Rdata")
