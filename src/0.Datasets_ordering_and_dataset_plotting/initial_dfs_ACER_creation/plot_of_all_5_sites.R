#Plot for presentation

library(maps)
library(graphics)
library(raster)
library(rgdal) #for axes plotting
library(grDevices)


basedrive = '/Users/annarella/Dropbox'

mondo <- brick(paste0(basedrive,"/sirius/naturalearth/HYP_HR_SR_OB_DR/HYP_HR_SR_OB_DR.tif"))
load(paste0(basedrive,"/sirius/datasets/ACER_data/MT_sites.Rda"))

convert_longs <-function(longitudes){
  long <- longitudes
  for (l in c(1:length(longitudes))){
    if(longitudes[l] < -178 & !is.na(longitudes[l])){
      long[l] <- longitudes[l] + 360
    }
  }
}

c <- MT_sites$site_name
c[4] <- "         Little Lake" 
c[5] <- "           Fargher Lake"


plotRGB(mondo,ext = extent(-180,180,20,84), interpolate= TRUE, axes=FALSE, xlab="Latitude",ylab="Longitude", main="North American Modern Pollen Database sites, Fossil ACER sites", cex.main=2, cex.lab=1.8)
points(x = MT_sites$long,y = MT_sites$lat,cex=1,pch=3,lwd = 2,col="Maroon")
text(x = MT_sites$long, y = MT_sites$lat, labels=c, cex= 1, pos=c(1,1,1,1,3), offset = 0.5, col="black")

