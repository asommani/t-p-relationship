#### Zonal plots 
library(plot.matrix)

plot.matrix.spectral <- F
if (plot.matrix.spectral){ #this plots don't make much sense... the values doesn't change so much!
  ts_latitudes <- apply(apply(tmp.annual,c(2,3),mean,na.rm=T),2,rev)
  ps_latitudes <- apply(apply(pre.annual,c(2,3),mean,na.rm=T),2,rev)
  vap_latitudes <- apply(apply(vap.annual,c(2,3),mean,na.rm=T),2,rev)
  ps_latitudes_sum <- apply(apply(pre.annual,c(2,3),sum,na.rm=T),2,rev)
  
  ts_latitudes_sd <- apply(apply(tmp.annual,c(2,3),sd,na.rm=T),2,rev)
  ps_latitudes_sd <- apply(apply(pre.annual,c(2,3),sd,na.rm=T),2,rev)
  vap_latitudes_sd <- apply(apply(vap.annual,c(2,3),sd,na.rm=T),2,rev)
  ts_sd_lat_means <- apply(ts_latitudes_sd,1,mean,na.rm=T) # temporal mean of standard dev?
  vap_sd_lat_means <- apply(vap_latitudes_sd,1,mean,na.rm=T)
  ps_sd_lat_means <- apply(ps_latitudes_sd,1,mean,na.rm=T)
# Zonal variability timeseries
pdf(paste0(plotdrive,"/Zonal_and_Lon_trends/zonal_T_P_vap_variability_timeseries.pdf"),width = 7, height = 10)
par(mfrow=c(3,1))
par(family='serif')
par(mar=c(2.1,2.1,3.1,4.1))
plot(ts_latitudes_sd/ts_sd_lat_means,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of mean annual temperature per year \n normalized per latitude")
plot(vap_latitudes_sd/vap_sd_lat_means,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of mean annual vapor pressure per year \n normalized per latitude")
n <- ps_latitudes_sd/ps_sd_lat_means
n[which(n>2, arr.ind = T)] <- NA
plot(n,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of annual precipitation per year \n normalized per latitude" )
plot(ts_latitudes_sd,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of mean annual temperature per year")
plot(vap_latitudes_sd,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of mean annual vapor pressure per year")
plot(ps_latitudes_sd,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal standard deviation of annual precipitation per year" )
dev.off()
rm(ts_latitudes_sd,ps_latitudes_sd,vap_latitudes_sd,ts_sd_lat_means,ps_sd_lat_means,vap_sd_lat_means)

# Zonal mean timeseries 

pdf(paste0(plotdrive,"/Zonal_and_Lon_trends/zonal_T_P_timeseries.pdf"),width = 8, height = 10)
par(mfrow=c(2,1))
par(mar=c(2.1,2.1,3.1,4.1))
plot(ts_latitudes,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal mean annual temperature" )
plot(ps_latitudes,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal annual precipitation" )
dev.off()


pdf(paste0(plotdrive,"/Zonal_and_Lon_trends/zonal_T_P_V_timeseries.pdf"),width = 8, height = 10)
par(mfrow=c(2,1))
par(mar=c(2.1,2.1,3.1,4.1))
plot(ts_latitudes/apply(ts_latitudes,1,mean,na.rm=T),border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal mean annual temperature" )
plot(ps_latitudes,border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal annual precipitation" )
dev.off()
}

# ts_latitudes <- apply(apply(scaledT,c(2,3),mean,na.rm=T),2,rev)
# ps_latitudes <- apply(apply(scaledP,c(2,3),mean,na.rm=T),2,rev)
# vap_latitudes <- apply(apply(scaledV,c(2,3),mean,na.rm=T),2,rev)
#ps_latitudes_sum <- apply(apply(scaledP,c(2,3),sum,na.rm=T),2,rev)

#### For plot:
# mean along latitudes, no nees to weight
ts_latitudes <- apply(apply(tmp.annual,c(2,3),mean,na.rm=T),2,rev)
ps_latitudes <- apply(apply(pre.annual,c(2,3),mean,na.rm=T),2,rev)
vap_latitudes <- apply(apply(vap.annual,c(2,3),mean,na.rm=T),2,rev)

# vector of mean of the latitudinal timeseries
ts_latitudes_means <- apply(ts_latitudes,1,mean,na.rm=T)
ps_latitudes_means <- apply(ps_latitudes,1,mean,na.rm=T)
vap_latitudes_means <- apply(vap_latitudes,1,mean,na.rm=T)

# range(ts_latitudes- ts_latitudes_means,na.rm=T)
# hist(ts_latitudes - ts_latitudes_means)
# 
# range(ps_latitudes- ps_latitudes_means,na.rm=T)
# hist(ps_latitudes- ps_latitudes_means)
# 
# range(vap_latitudes- vap_latitudes_means,na.rm=T)
# hist(vap_latitudes- vap_latitudes_means)

pdf(paste0(plotdrive,"/Zonal_and_Lon_trends/zonal_T_P_timeseries_centered_on_lat_mean.pdf"),width = 8, height = 10)
par(mfrow=c(3,1))
par(mar=c(2.1,2.1,3.1,4.1))
#axis.col=NULL, axis.row=NULL
breaks = seq(-3.7,3.7,length.out = 21)
plot(ts_latitudes-ts_latitudes_means,border=NA, xlab='', ylab='',
     breaks=breaks,col=col_ipcc_temperature(21)[-11], main = "Latitudinal mean annual temperature centered on lat mean [Celsius]" )

breaks = seq(-1.3,1.3,length.out = 21)
plot(vap_latitudes- vap_latitudes_means,border=NA, xlab='', ylab='',
     breaks=breaks,col=col_ipcc_temperature(21)[-11], main = "Latitudinal mean annual vapor pressure centered on lat mean [hPA]" )

breaks = c(-620,-400,-300,seq(-200,200,length.out =15),300,400,620)
plot(ps_latitudes-ps_latitudes_means,border=NA, xlab='', ylab='',
     breaks=breaks,col=col_ipcc_temperature(21)[-11], main = "Latitudinal mean annual precipitation centered on lat mean [mm/yr]" )
dev.off()
rm(ps_latitudes_means,ts_latitudes_means,vap_latitudes_means)


#ts_lons <- apply(tmp.annual,c(1,3),weighted.mean,cos(lat*pi/180),na.rm=T)
#ps_lons <- apply(pre.annual,c(1,3),weighted.mean,cos(lat*pi/180),na.rm=T)
# pdf(paste0(plotdrive,"/longitudinal_T_P_timeseries.pdf"),width = 8, height = 10)
# par(mfrow=c(2,1))
# par(mar=c(2.1,2.1,3.1,4.1))
# plot(apply(ts_lons, 2, rev),border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal mean annual temperature" )
# plot(apply(ps_lons, 2, rev),border=NA, col=colorRampPalette(brewer.pal(11,"Spectral"))(120), main = "Latitudinal annual precipitation" )
# dev.off()