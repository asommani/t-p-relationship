#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
#basedrive="/stacywork"

basedrive="/Users/annarella/Dropbox"
library(ncdf4)
elev <- read.table(paste0(basedrive,'/sirius/datasets/Modern_climates/CRU/CRU3.0_elevation/halfdesg.elv.grid.data'))
nc_tmp <- nc_open(paste0(basedrive,'/sirius/datasets/Modern_climates/CRU/CRU_TS_404/cru_ts4.04.1901.2019.tmp.dat.nc'))
library(raster) # package for raster manipulation
library(rgdal) # package for geospatial analysis
library(ggplot2) # package for plotting

lon <- ncvar_get(nc_tmp, "lon")
lat <- ncvar_get(nc_tmp, "lat")
time <- ncvar_get(nc_tmp,"time") #monthly means from 2011 to 2019
tmp.array <- ncvar_get(nc_tmp,"tmp")
dim(tmp.array) #lon #lat #time
nc_close(nc_tmp) 
tmp.slice <- tmp.array[,,103] #July 2019
year2019<- tmp.array[,,97:108]
dim(year2019)
tmp2019 <- apply(year2019,c(1,2),mean, na.rm=TRUE)
dim(tmp2019)
r <- raster(t(tmp2019), xmn=min(lon), xmx=max(lon), ymn=min(lat), ymx=max(lat), crs=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs+ towgs84=0,0,0"))
r <- flip(r, direction='y')
#png(paste0(plotdrive_temp,"/tmp.png"), width = 654, height =505)
par(family = 'serif')
plot(r, main="Annual mean temperature 2019") #, xlab= 'Longitude', ylab = 'Latitude') #it's raster plot!
text(x = 247, y = 0, labels = "[mm/day]", xpd = NA,srt = 270, cex = 1)




r <- raster(elev, xmn=min(lat), xmx=max(lat), ymn=min(lon), ymx=max(lon), crs=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs+ towgs84=0,0,0"))

plot(elev)
relev <- raster(elev)
r <- raster(elev, xmn=0, xmx=720, ymn=0, ymx=360, crs=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs+ towgs84=0,0,0"))
elev[elev == -999]<- NA
elev <- as.matrix(elev)
r <- raster(elev, xmn=min(lon), xmx=max(lon), ymn=min(lat), ymx=max(lat), crs=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs+ towgs84=0,0,0"))
r <- flip(r, direction='y')

pdf(paste0(basedrive,"/sirius/plots/CRU_plots/Elevation3.0/elevation_from_halfdesg.elv.grid.data.pdf"),width = 7, height = 5)
par(family = 'serif')
plot(r, col =terrain.colors(255)[-c(1:100)], main='Elevation')
text(x = 300, y = 0, labels = "[m]", xpd = NA,srt = 270, cex = 1)
dev.off()
