
basedrive="/Users/annarella/Dropbox"
plotdrive = paste0(basedrive,"/sirius/plots/CRU_plots")
library(ncdf4)
nc_PET <- nc_open(paste0(basedrive,'/sirius/datasets/Modern_climates/CRU/PET_VAT_CRU_TS_v4.04_1961-1990/cru_ts4.04.2011.2019.pet.dat.nc'))
library(raster) # package for raster manipulation
library(rgdal) # package for geospatial analysis
library(ggplot2) # package for plotting

lon <- ncvar_get(nc_PET, "lon")
lat <- ncvar_get(nc_PET, "lat")
time <- ncvar_get(nc_PET,"time") #monthly means from 2011 to 2019
pet.array <- ncvar_get(nc_PET,"pet")
dim(pet.array) #lon #lat #time
nc_close(nc_PET) 


pet.slice <- pet.array[,,103] #July 2019
year2019<- pet.array[,,97:108]
dim(year2019)
PET2019 <- apply(year2019,c(1,2),mean, na.rm=TRUE)
dim(PET2019)
r <- raster(t(PET2019), xmn=min(lon), xmx=max(lon), ymn=min(lat), ymx=max(lat), crs=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs+ towgs84=0,0,0"))

r <- flip(r, direction='y')
#png(paste0(plotdrive_temp,"/PET.png"), width = 654, height =505)
par(family = 'serif')
plot(r, main="Annual mean potential evapotranspiration for 2019") #, xlab= 'Longitude', ylab = 'Latitude') #it's raster plot!
text(x = 247, y = 0, labels = "[mm/day]", xpd = NA,srt = 270, cex = 1)

#dev.off()
###### Add continents

#####

mtext('aaaa', side = 4, padj =6.5)

