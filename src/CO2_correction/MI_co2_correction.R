#MI correction
#rm(list = ls(all.names = TRUE)) #will clear all objects including hidden objects.
#gc() #free up memrory and report the memory usage.
#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive = '/Users/annarella/Dropbox'
library(PaleoSpec)
library(zoo)
mis_transitions <- c(0  ,14700 , 27800,  59400,  73500, 130000)
miny = mis_transitions[2];maxy = mis_transitions[5]
xlims <- c(maxy,miny)
source(paste0(basedrive,'/sirius/src/utils/read_recons_dbs.R'))
# firstlist, foss.meta, foss.poll, foss.times, modern.clim.db, modern.poll.db, sites, training_ranges, vartoreconstr
# fp <- time_restriction(foss.poll[[s]], min.year = miny,max.year = maxy)
# fcoord <- foss.meta[[s]]$coord
# mp <- modern.poll.db[[s]]
# mc <- modern.clim.db[[s]]
# dists <- seq(training_ranges[[s]][1],training_ranges[[s]][2], by=50)

##### MI correction
if (file.exists(file=paste0(basedrive,'/sirius/data/Processed_data/recons/MI_corrections_simple.Rdata'))){
  load(file=paste0(basedrive,'/sirius/data/Processed_data/recons/MI_corrections_simple.Rdata'))
} else {
  
  CO2_data <- read.table(paste0(basedrive,'/sirius/data/Raw_data/Koehler-etal_2017/CO2_stack_156K_spline_V2.tab') , skip = 13)
  colnames(CO2_data) <-c('Age [ka BP]',	'CO2 [µmol/mol]','CO2 std dev [±]','dR_[CO2] [W/m**2]','dR_[CO2] std dev [±]')
  
  cotimemask <- (CO2_data$`Age [ka BP]` > floor(miny*0.001) & CO2_data$`Age [ka BP]` < ceiling(maxy*0.001))
  CO2_data_res <- CO2_data[cotimemask,]
  CO2 <- zoo(CO2_data_res$`CO2 [µmol/mol]`,order.by = CO2_data_res$`Age [ka BP]`)
  # functions for CO2 correction
  source(paste0(basedrive,'/sirius/src/Sandy_CO2_code/Mine_co2_correction_commented.R'))
  #import data fossil modern pollen
  
 MI_corrections_all_sites <- list()
  MI_corrections_all_sites_expanded<-list()
  for (s in sites){
    print(s)
    reconstructions_with_errors <- readRDS(file = paste0(basedrive,'/sirius/data/Processed_data/recons/',s,'_reconstructions_with_errors.rds'))
    if (is.null(MI_corrections_all_sites[[s]])){
      MI_correcion_models <- list()
      MI_correcion_models_expanded <-list()
      for (m in names(reconstructions_with_errors)){
        dumblist_for_use <- list()
        print(m)
        # Temperature and CO2 reference
        T_ref <- foss.meta[[s]]$modern.clim.var.at.fossil.site['Tann',1]
        MI_recs<-  reconstructions_with_errors[[m]][['MI']][['rec']]
        ft<- as.numeric(colnames(MI_recs))*0.001
        CO2_eq <- zoo(PaleoSpec::MakeEquidistant(index(CO2),CO2,time.target = ft),order.by=ft)
        c_ratio <- CO2_eq/340
        T_rec <- reconstructions_with_errors[[m]][['Tann']][['rec']]['pred',]
        T_diff <- T_ref - T_rec
        MI_cors_expanded <- array(dim=c(dim(MI_recs),3),
                         dimnames = list(dimnames(MI_recs)[[1]],dimnames(MI_recs)[[2]],c("m_true", "m_delta", "comp_point")))
        MI_cors <- array(dim=dim(MI_recs), dimnames = dimnames(MI_recs))
        # create array
        for(value in dimnames(MI_recs)[[1]]){
          print(value)
          for (i in c(1:length(T_diff))){
            MI_cors_expanded[value,i,] <- unlist(mi_correction(m_rec=MI_recs[value,i],T_diff[i],T_ref=T_ref,c_ratio=c_ratio[i]))
            } # i loop, MI correction
            MI_cors[value,] <- MI_cors_expanded[value,,'m_true'] 
            } # End of array, median, 95%, 5% loop
          dumblist_for_use[['MI']]<- list(rec=MI_cors)
          MI_correcion_models[[m]] <- dumblist_for_use
          MI_correcion_models_expanded[[m]] <- MI_cors_expanded
          } # loop on the models
      MI_corrections_all_sites[[s]]<- MI_correcion_models
      MI_corrections_all_sites_expanded[[s]] <- MI_correcion_models_expanded
      } ##
    } ## loop on sites
  
  save(MI_corrections_all_sites,MI_corrections_all_sites_expanded, file=paste0(basedrive,'/sirius/data/Processed_data/recons/MI_corrections_simple.Rdata'))
} # if no object


#### correct the MAT values, add 'cv'
load(file=paste0(basedrive,'/sirius/data/Processed_data/recons/MI_corrections_simple.Rdata'))

for (s in sites){
reconstructions_with_errors <- readRDS(file = paste0(basedrive,'/sirius/data/Processed_data/recons/',s,'_reconstructions_with_errors.rds'))
# take errs, the RMSEP/1-M (median of sq distances):
errs <- reconstructions_with_errors[['MAT']][['MI']][['rec']]['pred',]-reconstructions_with_errors[['MAT']][['MI']][['rec']]['lowval',]

MI_corrections_all_sites[[s]][["MAT"]][['MI']][["rec"]]["highval",] <- errs + MI_corrections_all_sites[[s]][["MAT"]][['MI']][["rec"]]["pred",]
MI_corrections_all_sites[[s]][['MAT']][['MI']][['rec']]["lowval",] <- -errs + MI_corrections_all_sites[[s]][["MAT"]][['MI']][["rec"]]["pred",]
for (m in names(reconstructions_with_errors)){
  MI_corrections_all_sites[[s]][[m]][['MI']][["cv"]] <- reconstructions_with_errors[[m]][['MI']][['cv']]
}
}
saveRDS(MI_corrections_all_sites, file=paste0(basedrive,'/sirius/data/Processed_data/recons/MI_corrections.rds'))



source(paste0(basedrive,'/sirius/src/functions/plot_fun_for_reconstructions_thesis.R'))
v <- 'MI'
par(mfrow=c(1,1))
ylims <- find_shared_ylim_MI_corr(s,v,m,foss.meta,reconstructions_with_errors,MI_corrections_at_site)

plot_recons_with_error(s=s,v=v,m=m, text=v, #Qanalogue = anal_qualities_in_the_sites,
                       reconstructions_with_errors = reconstructions_with_errors, foss.meta = foss.meta,
                       shifttext=1, xlims = c(73500,14700),ylims = ylims)
plot_recons_with_error(s=s,v=v,m=m, text=v, #Qanalogue = anal_qualities_in_the_sites,
                       reconstructions_with_errors = MI_corrections_all_sites[[s]], foss.meta = foss.meta,
                       shifttext=1, xlims = c(73500,14700), ylims = ylims, is.correction = T,add = T)
# yo!


# curiosity, difference is different! anyway I will use my RMSEP
#MI_corrections_all_sites[[s]][['MAT']]['highval',,'m_true']-MI_corrections_all_sites[[s]][['MAT']]['pred',,'m_true']
# Nils's:
# wapls_ensembles_mi_corrected <- ensemble_mi_correction <- array(0,dim=dim(wapls_ensembles_mi))
# lat = -20 # Latitude
# for (i in 1:2000) {
#   T_ref <- wapls_ensembles_ts[1,i]
#   T_diff <- wapls_ensembles_ts[,i]
#   c_ratio <- co2_concentration/340
#   m_rec <- wapls_ensembles_mi[,i]
#   wapls_ensembles_mi_corrected[,i] <- sapply(1:length(T_diff),function(i) mi_correction(m_rec=m_rec[i],T_diff=T_diff[i],T_ref=T_ref,c_ratio=c_ratio[i],lat=lat,modern_CO2=340)$m_true)
#   ensemble_mi_correction[,i] <- wapls_ensembles_mi_corrected[,i] - m_rec
# }
