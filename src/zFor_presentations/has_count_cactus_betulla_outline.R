#basedrive="/stacywork"
#basedrive="/mnt/ssh/dumbledore/stacywork"
basedrive="/Users/annarella/OneDrive/STACY_Tesi"
plotdrive_temp = paste0(basedrive,"/sirius/temp")
pollenpcndrive = "/Users/annarella"

library(pollenpcn)
library(plotrix)

load(paste0(basedrive,"/sirius/datasets/SHL_data/moderndata.RData"))
load(paste0(basedrive,"/sirius/datasets/ACER_data/MT_sites.Rda"))

matrix_binaria <- foss.poll

matrix_binaria[foss.poll != 0] = 1
color2D.matplot(matrix_binaria[c(1:200),c(1:82)], show.legend=TRUE, main="has count n/y (0,1)", xlab='taxa', ylab="first 200 sample")
sum(!matrix_binaria)/(dim(matrix_binaria)[1]*length(matrix_binaria)) #tot number of elements
#0.6987909 % sparcity
# 82 taxa


load.configuration(path= paste0(pollenpcndrive,"/pollenpcn-data"))
ACER <- ACERdataset()

Monticchio.pollen <- ACER$db$pollen_data[ACER$db$pollen_data$site_id==37,]
summary(factor(Monticchio.pollen$taxon))
Monticchio.binary = Monticchio.pollen$taxon_count
Monticchio.binary[Monticchio.pollen$taxon_count!=0] = 1
sum(!Monticchio.binary)/length(Monticchio.binary)
# 0.827952 sparcity
length(levels(factor(Monticchio.pollen$taxon)))
# 164 taxa

Tulane.pollen <- ACER$db$pollen_data[ACER$db$pollen_data$site_id==24,]
#summary(factor(Tulane.pollen$taxon))
Tulane.binary = Tulane.pollen$taxon_count
Tulane.binary[Tulane.pollen$taxon_count!=0] = 1
sum(!Tulane.binary)/length(Tulane.binary)
#0.840136 sparcity
length(levels(factor(Tulane.pollen$taxon)))
# 192 taxa

littlelake.pollen <- ACER$db$pollen_data[ACER$db$pollen_data$site_id==27,]
#summary(factor(littlelake.pollen$taxon))
littlelake.binary = littlelake.pollen$taxon_count
littlelake.binary[littlelake.pollen$taxon_count!=0] = 1
sum(!littlelake.binary)/length(littlelake.binary)
#0.6080473 sparcity
length(levels(factor(littlelake.pollen$taxon)))
# 65 taxa


Fahrger.pollen <- ACER$db$pollen_data[ACER$db$pollen_data$site_id==7,]
#summary(factor(Fahrger.pollen$taxon))
Fahrger.binary = Fahrger.pollen$taxon_count
Fahrger.binary[Fahrger.pollen$taxon_count!=0] = 1
sum(!Fahrger.binary)/length(Fahrger.binary)
#0.5280313 sparcity
length(levels(factor(Fahrger.pollen$taxon)))
# 59 taxa


# SHL  37  24  27  7   #sites
# 70% 83% 84% 60% 65% #sparciticy
# 82  164 192 65  59  #no.taxa
names<- MT_sites$site_name
names[2] <- "Monticchio"
sparc <- c(70,83,84,60,65)
no.taxa <- c(82,164,192,65,59)
no.samplestime <- c(1293,542,192,130,130)

#par(bg=NA)
plot(no.taxa,sparc, xlab = "number of taxa", ylab = "sparsity %",xlim = c(40,300), bg=NA, cex.lab=1.3,cex.main=1.4, main="Proxy data sparsity" )
text(x = no.taxa, y = sparc, labels=names, cex= 1.1, pos=4,offset=0.6)
#dev.copy(png,'sparsbgg.png', width=340, height=400)
#dev.off()
#dev.off()

plot(no.samplestime,sparc)



########## fossi calctus
cactus_perc <- c(80,70,50,15,12,8,9,2,5,2,1,1,0)
pann <- c(1,20,30,70,100,130,200, 270,300,350,380,400,410)
plot(pann,cactus_perc)
scatter.smooth(pann,cactus_perc)
lmod <-lm(cactus_perc ~ pann) # y ~ x
ylm <- lmod$coefficients[2]*pann + lmod$coefficients[1]
lines(pann,ylm,col="green" )

lmod2 <- lm(cactus_perc ~ pann + I(pann^2))
ylm2 <- lmod2$coefficients[3]*pann*pann + lmod$coefficients[2]*pann + lmod$coefficients[1]
lines(pann,predict(lmod2,pann),col="red" )

y = cactus_perc
x =pann
#fit first degree polynomial equation:
fit  <- lm(y~x)
#second degree
fit2 <- lm(y~poly(x,2,raw=TRUE))
#third degree
fit3 <- lm(y~poly(x,3,raw=TRUE))
#fourth degree
fit4 <- lm(y~poly(x,4,raw=TRUE))
fit5 <- lm(y~log(x))
fdecay <- function(x, a,b,c){a*exp(b*x)+c}
library(minpack.lm)
fit6 <- nlsLM(y ~ fdecay(x, a, b,c), start = list(a =1, b = -1,c=10), trace = T, control = list(warnOnly = T, minFactor = 1/2048))
library(segmented)
#piecewise linear regression
segmented.mod <- segmented(fit, seg.Z = ~x, psi=14, npsi=2)

xx <- seq(0,400, length=50)
plot(x,y,pch=19,ylim=c(1,90), xlab="Pann [mm]",ylab="Cactus %", main="Predict catus from precipitarion %",cex.lab=1.3,cex.main=1.4) 
lines(x,y,"l",col="orange") #child play
lines(xx, predict(fit, data.frame(x=xx)), col="red") #linear regression
plot(segmented.mod, add=T,col="blue") #piecewise linear regression
lines(xx, predict(fit6, data.frame(x=xx)), col="green") # exponential
lines(xx, predict(fit4, data.frame(x=xx)), col="purple") #fourth order polinomial

lines(xx, predict(fit2, data.frame(x=xx)), col="orange") #parabola
lines(xx, predict(fit5, data.frame(x=xx)), col="maroon") #base exp
lines(xx, predict(fit3, data.frame(x=xx)), col="cyan") #cubic poly




######################### inverse modelling

x = cactus_perc
y = pann
#fit first degree polynomial equation:
fit  <- lm(y~x)
#second degree
fit2 <- lm(y~poly(x,2,raw=TRUE))
#third degree
fit3 <- lm(y~poly(x,3,raw=TRUE))
#fourth degree
fit4 <- lm(y~poly(x,4,raw=TRUE))

fdecay <- function(x, a,b,c){a*exp(b*x)+c}
library(minpack.lm)
fit6 <- nlsLM(y ~ fdecay(x, a, b,c), start = list(a =1, b = -1,c=10), trace = T, control = list(warnOnly = T, minFactor = 1/2048))
library(segmented)
#piecewise linear regression
segmented.mod <- segmented(fit, seg.Z = ~x, psi=14, npsi=2)

width=340
height=400
xx <- seq(0,100, length=50)
dev.off()
par(bg=NA)
plot(x,y,pch=19,xlim=c(0,90), ylab="Pann [mm]",xlab="Cactus %", main="Predict precipitation from cactus %",cex.lab=1.4,cex.main=1.4) 
dev.copy(png,paste0(plotdrive_temp,'/cactus0.png'), width=width, height=height)
dev.off()
#lines(x,y,"l",col="orange") #child play
lines(xx, predict(fit, data.frame(x=xx)), col="red") #linear regression
dev.copy(png,paste0(plotdrive_temp,'/cactus1.png'), width=width, height=height)
dev.off()
lines(xx, predict(fit6, data.frame(x=xx)), col="green") # exponential
dev.copy(png,paste0(plotdrive_temp,'/cactus2.png'), width=width, height=height)
dev.off()
lines(xx, predict(fit4, data.frame(x=xx)), col="purple") #fourth order polinomial
dev.copy(png,paste0(plotdrive_temp,'/cactus3.png'), width=width, height=height)
dev.off()
plot(segmented.mod, add=T,col="blue") #piecewise linear regression
dev.copy(png,paste0(plotdrive_temp,'/cactus4.png'), width=width, height=height)
dev.off()
dev.off()

lines(xx, predict(fit2, data.frame(x=xx)), col="orange") #parabola
lines(xx, predict(fit3, data.frame(x=xx)), col="cyan") #cubic poly


################################ real exmpl na merda
plot(mod.poll$Acer[mod.clim$Tann<20 & mod.clim$Tann>0], mod.clim$Tann[mod.clim$Tann<20 & mod.clim$Tann>0], ylim=c(0,20))

dev.off()
par(bg=NA)
plot(mod.poll$Betula..undiff..,mod.clim$Pann, xlab = "Betula (birch) %", ylab = "Pann [mm]", main="Reality",cex.lab=1.4,cex.main=1.4)
dev.copy(png,paste0(plotdrive_temp,'/reality.png'), width=width, height=height)
dev.off()
dev.off()

fitbetula <- lm(mod.clim$Pann~mod.poll$Betula..undiff..)
xx <- seq(0,100, length=80)
lines(xx, fitbetula$coefficients[2]*xx+fitbetula$coefficients[1])
lines(xx, predict(fitbetula, data.frame(x=xx)), col="red") #linear regression


# fit random function :
flala <- function(x, a, b,c,d,e) {a + b*x/(c+x) + d*x^2 + e*x^3}
#nolfit <- nls(y ~ flala(x, a, b, c, d), start = list(a =1, b = 1,c=1, d=1), trace = T, control = list(warnOnly = T, minFactor = 1/2048))
nolfit0<-nlsLM(y ~ flala(x, a, b,c,d,e), start = list(a =1, b = 1,c=1,d=1,e=1), trace = T, control = list(warnOnly = T, minFactor = 1/2048))
plot(x,y,pch=19,ylim=c(1,100))
lines(xx, predict(nolfit0, data.frame(x=xx)), col="orange")
#library(minpack.lm)
#nolfit<-nlsLM(y ~ flala(x, a, b, c, d), start = list(a =1, b = 1,c=1, d=1), trace = T, control = list(warnOnly = T, minFactor = 1/2048))



