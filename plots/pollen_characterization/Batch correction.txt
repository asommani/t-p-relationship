Batch correction:
limma package:
two-way ANOVA-based batch centering (classic approach):

Batch adjustment using two-way ANOVA to estimate batch effects
Removing batch effects while retaining group differences can be achieved by subtracting the batch effects equation M18 estimated from the two-way ANOVA model (2.1), yielding equation M19. This gives batch-adjusted values, where any systematic bias induced by the batch differences has been removed, while the group differences are retained.

The estimation error equation M20 affects all values within the same batch in the same manner. Thus, while the aim is to remove spurious dependencies within batches, it may also induce new dependencies. The batch effect estimation errors will influence group effects in proportion to how well the group is represented in each batch:

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4679072/