# Temperature-precipitation relationship from pollen-based climate reconstructions and simulations during the Last Glacial. 

This repository contains code, plots, and data output produced during my master thesis research work with title "Temperature-precipitation relationship from pollen-based climate reconstructions and simulations during the Last Glacial". The pdf of the master thesis can be found here  https://heibox.uni-heidelberg.de/f/28b342b6a1464c8e8514/.

The project directory is organized as following:

**data** contains the tables (.cvs) or R objects (.Rds, Rda) of the data used and processed in the thesis (without the raw data).

**plots** contains the plots produced during the thesis work. The most interesting folders are *CRU plots* (CRU data analysis), *determine_training_range* (SQ distances), *MAT_video* (snapshots of best modern analogues), *plot_thesis* (images for thesis) and *randomtf* (Proportion of variance in the fossil data explained by an environmental reconstruction from the paleosig package).

**code** contains the code used in the master thesis, the naming convention is similar as for the plots (e.g. plot thesis folder as the code for the images used in the thesis).

**temp** has temporary material (e.g. checks or hacks).

**code_projects** and **Markdown_projects** contain subproject, carried out for curiosity, data processing or conferences. 

The following pubblication is based on this work:
Sommani, A., Weitzel, N., and Rehfeld, K.: Northern Hemisphere temperature to precipitation relationships during the last Glacial from pollen records and climate simulations, EGU 2020, https://doi.org/10.5194/egusphere-egu2020-7293.