@incollection{sanchezgoi2017hnft,
 author={Maria Fernanda {Sanchez Go\~{n}i} and St\'{e}phanie {Desprat} and Anne-Laure {Daniau} and Franck C {Bassinot} and Josu\'{e} M {Polanco-Mart\'{\i}nez} and Sandy P {Harrison}},
 title={{Harmonized names for the pollen taxa, link to a list in different formats (zipped)}},
 year={2017},
 doi={10.1594/PANGAEA.880501},
 url={https://doi.org/10.1594/PANGAEA.880501},
 note={In supplement to: Sanchez Go\~{n}i, Maria Fernanda; Desprat, St\'{e}phanie; Daniau, Anne-Laure; Bassinot, Franck C; Polanco-Mart\'{\i}nez, Josu\'{e} M; Harrison, Sandy P; Allen, Judy R M; Anderson, R Scott; Behling, Hermann; Bonnefille, Raymonde; Burjachs, Francesc; Carri\'{o}n, Jos\'{e} S; Cheddadi, Rachid; Clark, James S; Combourieu-Nebout, Nathalie; Courtney Mustaphi, Colin J; DeBusk, Georg H; Dupont, Lydie M; Finch, Jemma M; Fletcher, William J; Giardini, Marco; Gonz\'{a}lez, Catalina; Gosling, William D; Grigg, Laurie D; Grimm, Eric C; Hayashi, Ryoma; Helmens, Karin F; Heusser, Linda E; Hill, Trevor R; Hope, Geoffrey; Huntley, Brian; Igarashi, Yaeko; Irino, Tomohisa; Jacobs, Bonnie Fine; Jim\'{e}nez-Moreno, Gonzalo; Kawai, Sayuri; Kershaw, A Peter; Kumon, Fujio; Lawson, Ian T; Ledru, Marie-Pierre; L\'{e}zine, Anne-Marie; Liew, Ping-Mei; Magri, Donatella; Marchant, Robert; Margari, Vasiliki; Mayle, Francis E; McKenzie, G Merna; Moss, Patrick T; M\"{u}ller, Stefanie; M\"{u}ller, Ulrich C; Naughton, Filipa; Newnham, Rewi M; Oba, Tadamichi; P\'{e}rez-Obiol, Ramon P; Pini, Roberta; Ravazzi, Cesare; Roucoux, Katherine H; Rucina, Stephen M; Scott, Louis; Takahara, Hikaru; Tzedakis, Polychronis C; Urrego, Dunia H; van Geel, Bas; Valencia, Bryan G; Vandergoes, Marcus J; Vincens, Annie; Whitlock, Cathy L; Willard, Debra A; Yamamoto, Masanobu (2017): The ACER pollen and charcoal database: a~global resource to document vegetation and fire response to abrupt climate changes during the last glacial period. Earth System Science Data, 9(2), 679-695, https://doi.org/10.5194/essd-9-679-2017},
 type={data set},
 publisher={PANGAEA}
}
megabiome	char_taxa
Temperate savannah	Anacardiaceae, Ericaceae, Euphorbiaceae, Fabaceae, Fabaceae (Acacia), Proteaceae
Warm-temperate mixed Forest	Apocynaceae, Celastraceae, Combretaceae, Cyanthaceae, Erythroxylaceae, Flacourtiaceae, Moraceae, Myricaceae, Myrtaceae, Podocarpaceae, Rosaceae, Rubiaceae
